extends m_generic

@onready var g_animationtree = $animation/tree
@onready var g_state = g_animationtree["parameters/playback"]

@export var fire: int = 1				# NUMBER OF FIRES
@export var life: int = 4				# LIFE

#======== GENERIC CALLBACK =====================================================
func on_hit_cbk(_hit : GLOBAL.Hit):		g_state.travel("hit")
func on_die_cbk():						g_state.travel("die")
func on_end(): 							queue_free()

#======== ATTRIBUTES ===========================================================
var m_period		= 0					# CURRENT PERIOD
var m_hit_period	= 0					# STOP PERIOD
var m_angle			= 0					# RANDOM ANGLE FOR EACH PERIOD
var m_shoot			= false				# IS SHOOTING
const nb_period		= 3					# PERIOD CYCLE

#======== INITIALISATION =======================================================
func _ready():
	super()
	m_hit_period = GLOBAL.RNG.randi_range(0,nb_period-1)
	set_life_max(life) ; set_life(life)
	set_period(1) ; set_weight(0.8) ; set_speed(50) ; set_accel(50)
	m_hit.node = $hit

#======== MOVE: 0 STAY, 1-3 MOVE TO PLAYER, 4 MOVE RANDOMLY=====================
func _physics_process(_delta):
	if !is_active(): return
	super(_delta)
	var v_target = Vector2.ZERO
	var v_period = m_period % nb_period
	if is_alive() && (m_period>0) :
		if v_period == m_hit_period && GLOBAL.PLAYER!=null :
			if !m_shoot:
				m_shoot=true
				g_state.travel("shoot_"+str(fire))
		else:
			m_shoot=false
			v_target = Vector2(1,0).rotated(m_angle)
		
	velocity = velocity.move_toward(v_target*get_speed(), get_accel()*_delta)
	move_and_slide()

#======== ON PERIOD CALLBACK ===================================================
func on_period(_period)	:
	if is_alive():
		m_period = _period
		m_angle = GLOBAL.RNG.randf_range(0,2*PI)

#======== ON HIT ===============================================================
func _on_hit_area_entered(area): super(area) ; on_die()

#======== SHOOT ================================================================
func shoot():
	if !is_active() || GLOBAL.PLAYER==null : return
	
	var v_dir = (GLOBAL.PLAYER.position - position).normalized()
	var v_bullet = bullet_scene.instantiate()
	var v_position = Vector2.ZERO
	var v_height = 15
	v_position.y = position.y + v_dir.y*4
	v_position.x = position.x + v_dir.x*6
	v_bullet.set_initial_height(v_height)
	v_bullet.set_position(v_position)
	v_bullet.set_direction(v_dir)
	v_bullet.set_foe()
	v_bullet.set_speed(150)
	v_bullet.set_distance(150)
	if GLOBAL.MALUS.fire_size!=1: v_bullet.scale = Vector2(GLOBAL.MALUS.fire_size,GLOBAL.MALUS.fire_size)
	v_bullet.modulate = Color.WHITE
	get_parent().add_child(v_bullet)
