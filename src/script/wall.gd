extends m_generic

enum STATE { IDLE=0, MOVING=1, READY=2, FIRING=3 }
@export_enum("up","down") var where:String=		"up"
@export_enum("fly","ground") var type:String=	"fly"

@export var life: int = 9				# LIFE

#======== GENERIC CALLBACK =====================================================
func on_hit_cbk(_hit : GLOBAL.Hit):
	$animation/def.play(str(where)+".hit")
	m_lastperiod = m_period
func on_die_cbk():						$animation/def.play(str(where)+".die")
func on_end(): 							remove_from_group(GLOBAL.ENEMY_GROUP)

#======== ATTRIBUTES ===========================================================
var m_period		= 0					# CURRENT PERIOD
var m_lastperiod	= 0					# LAST ACTION PERIOD
const nb_period		= 5					# PERIOD CYCLE
var m_state:STATE	= STATE.IDLE		# WALL STATE
var m_dir:Vector2	= Vector2.ZERO		# DIRECTION VECTOR

#======== INITIALISATION =======================================================
func _ready():
	super()
	set_life_max(life) ; set_life(life)
	set_period(1);
	set_speed(300) ; set_accel(300)
	$animation/def.play(str(where)+".idle")
	
	if type=="fly":
		set_collision_layer_value(3,false)
		set_collision_layer_value(6,true)
		set_collision_mask_value(4, false)
		set_collision_mask_value(6, true)
	else:
		set_collision_layer_value(3,true)
		set_collision_layer_value(6,false)
		set_collision_mask_value(4, true)
		set_collision_mask_value(6, false)
		

#======== MOVE: 0 STAY, 1-3 MOVE TO PLAYER, 4 MOVE RANDOMLY=====================
func _physics_process(_delta):
	if !is_active(): return
	super(_delta)
	if is_alive() && !is_hit() :
		if (m_period>m_lastperiod):
			match(m_state):
				STATE.IDLE:
					if m_dir.x > 0:	$animation/def.play(str(where)+".right_start")
					else : $animation/def.play(str(where)+".left_start")
					m_state = STATE.MOVING
				STATE.MOVING:
					velocity = velocity.move_toward(m_dir*get_speed(), get_accel()*_delta)
					var v_stop = false
					var v_collision = move_and_collide(velocity*_delta)
					if v_collision: v_stop=true
					if GLOBAL.PLAYER && abs(GLOBAL.PLAYER.global_position.x - global_position.x)<5 : v_stop=true
					if v_stop:
						velocity = Vector2.ZERO
						if m_dir.x > 0:	$animation/def.play(str(where)+".right_stop")
						else : $animation/def.play(str(where)+".left_stop")
						m_state = STATE.READY
						m_lastperiod = m_period
				STATE.READY :
					$animation/def.play(str(where)+".fire_3")
					m_state = STATE.FIRING
					m_lastperiod = m_period
				STATE.FIRING:
					m_state = STATE.IDLE
					m_lastperiod = m_period
		
func shoot():
	if is_active():
		var v_bullet = bullet_scene.instantiate()
		var v_position = position
		var v_height = 15
		v_bullet.set_initial_height(v_height)
		if where=="up":
			v_position.y = v_position.y + 20
			v_bullet.set_direction(GLOBAL.DIRVEC[GLOBAL.DIR.DOWN])
		else:
			v_position.y = v_position.y - 5
			v_bullet.set_direction(GLOBAL.DIRVEC[GLOBAL.DIR.UP])
		v_bullet.set_position(v_position)
		v_bullet.set_foe()
		v_bullet.set_speed(200)
		v_bullet.set_distance(200)
		if GLOBAL.MALUS.fire_size!=1: v_bullet.scale = Vector2(GLOBAL.MALUS.fire_size,GLOBAL.MALUS.fire_size)
		v_bullet.modulate = Color.RED
		get_parent().add_child(v_bullet)

#======== ON PERIOD CALLBACK ===================================================
func on_period(_period)	:
	m_period = _period
	m_dir = Vector2.ZERO
	if GLOBAL.PLAYER :
		if GLOBAL.PLAYER.global_position.x > global_position.x :
			m_dir.x = 1
		else : m_dir.x = -1

