extends Control

@export_category("Settings")
@export var fps: int			= 60

@export_category("DebugKeys")
@export_enum("Dump_tree", "Dump_mob", "Freeze_mob", "Bomb", "Life", "Life_ex", "Life_max+", "Life_max-") var key_b: String = "Bomb"
@export_enum("Dump_tree", "Dump_mob", "Freeze_mob", "Friction", "Bomb", "Life", "Life_ex", "Life_max+", "Life_max-") var key_n: String = "Life"

@export_category("Stat")
@export var force: float		= 3
@export var armor: float		= 0
@export var luck: int			= 0
@export var nb_fires: int		= 1
@export var nb_back_fires: int 	= 0


@export_category("Bonus")
@export var fire_size: float	= 1
@export var speed: float		= 1
@export var bullet_speed: float	= 1
@export var map_all_visible:bool= false
@export var map_spe_visible:bool= false

@export_category("Malus")
@export var foe_fire_size: float	= 1



#========= INITIALISATION ======================================================
func _ready():
	GLOBAL.BONUS.fire_size			= fire_size
	GLOBAL.BONUS.speed				= speed
	GLOBAL.BONUS.bullet_speed		= bullet_speed
	GLOBAL.BONUS.map_all_visible	= map_all_visible
	GLOBAL.BONUS.map_spe_visible	= map_spe_visible
	
	GLOBAL.MALUS.fire_size			= foe_fire_size
	
	Engine.max_fps = fps

#========= DEBUG KEYS ==========================================================
func _process(_delta):
	$general.text = "NB NODES: "+str(get_tree().get_node_count()) + \
					" FPS: "+str(Engine.get_frames_per_second())
	
	$people.text = "FOES: "+str(get_tree().get_nodes_in_group(GLOBAL.ENEMY_GROUP).size())
	
	var keys = [{"key":"ui_debug_1","value":key_b},
				{"key":"ui_debug_2","value":key_n}]
	for i in keys.size() :
		if GLOBAL.PLAYER && Input.is_action_just_pressed(keys[i].key) :
			match keys[i].value :
				"Dump_tree":		get_tree().get_root().print_tree_pretty()
				"Dump_mob":
					if GLOBAL.PLAYER!=null : GLOBAL.PLAYER.dump()
					for mob in get_tree().get_nodes_in_group(GLOBAL.ENEMY_GROUP) : mob.dump()
				"Freeze_mob":		freeze(GLOBAL.ENEMY_GROUP)
				"Bomb":				GLOBAL.PLAYER.inc_nb_bombs(1)
				"Life":				GLOBAL.PLAYER.inc_life(1)
				"Life_ex":			GLOBAL.PLAYER.inc_life_ex(1)
				"Life_max+":		GLOBAL.PLAYER.inc_life_max(1)
				"Life_max-":		GLOBAL.PLAYER.dec_life_max(1)
				
#========= FREEZE ==============================================================
func freeze(_group):
	for mob in get_tree().get_nodes_in_group(_group) : mob.toggle_active()

#========= CONNET TO PLAYER ====================================================
func connect_to_player():
	print("[DBG] connect to player ("+str(GLOBAL.PLAYER)+")")
	if GLOBAL.PLAYER:
		GLOBAL.PLAYER.connect("update_stats", _on_pingmy_update_stats)
		GLOBAL.PLAYER.connect("pingmy_position", _on_pingmy_position)
		
		GLOBAL.PLAYER.set_force(force)
		GLOBAL.PLAYER.set_armor(armor)
		GLOBAL.PLAYER.set_luck(luck)
		GLOBAL.PLAYER.set_nb_fires(nb_fires)
		GLOBAL.PLAYER.set_nb_back_fires(nb_back_fires)

#========= PLAYER POSITION =====================================================
func _on_pingmy_position(_pos): $position.text = "POS: "+str(_pos)

#========= PLAYER STATISTIC UPDATE =============================================
func _on_pingmy_update_stats(_what, _data):
	var v_text = _what.to_upper()+": "+str(_data)
	match _what:
		"life": if GLOBAL.PLAYER: v_text=v_text+" ("+str(GLOBAL.PLAYER.get_life())+")"
	print("[DBG] "+v_text) ; $stats.text = v_text
	
