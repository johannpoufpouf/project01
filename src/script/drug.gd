extends m_generic

@onready var g_animationtree = $animation/tree
@onready var g_state = g_animationtree["parameters/playback"]

@export var life: int = 6				# LIFE

#======== GENERIC CALLBACK =====================================================
func on_hit_cbk(_hit : GLOBAL.Hit):		g_state.travel("hit")
func on_die_cbk():						g_state.travel("die")
func on_end(): 							queue_free()

#======== ATTRIBUTES ===========================================================
var m_dir			= Vector2.ZERO		# DRUG

#======== INITIALISATION =======================================================
func _ready():
	super()
	set_life_max(life) ; set_life(life)
	set_period(1); set_weight(1.2) ; set_speed(100) ;  set_accel(100)
	m_hit.node = $hit

#======== MOVE: 0 STAY, 1-3 MOVE TO PLAYER, 4 MOVE RANDOMLY=====================
func _physics_process(_delta):
	if !is_active(): return
	super(_delta)
	if is_alive():	velocity= velocity.move_toward(m_dir*get_speed(), get_accel()*_delta)
	else:			velocity= Vector2.ZERO
	move_and_slide()

#======== ON PERIOD CALLBACK ===================================================
func on_period(_period)	:
	if is_alive() && GLOBAL.PLAYER!=null :
		m_dir = (GLOBAL.PLAYER.position - position).normalized()

#======== ON HIT ===============================================================
func _on_hit_area_entered(area): super(area) ; on_die()
