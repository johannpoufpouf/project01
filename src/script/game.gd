extends PanelContainer

var start_scene	= preload("res://src/scene/ui/start.tscn")
var gui_scene   = preload("res://src/scene/ui/gui.tscn")
var level_scene	= preload("res://src/scene/level.tscn")
var map_scene	= preload("res://src/scene/ui/map.tscn")
var keypad_scene= preload("res://src/scene/ui/keypad.tscn")

var g_start			= null
var g_level			= null
var g_gui			= null
var g_map			= null
var g_keypad		= null

func _ready():
	g_start = start_scene.instantiate()
	g_start.connect("start_game",start_game)
	add_child(g_start)

func start_game():
	remove_child(g_start)
	
	GLOBAL.clean_stat()
	GLOBAL.clean_bonus_malus()

	g_gui = gui_scene.instantiate()
	g_gui.position=Vector2(17,17)
	add_child(g_gui)
	
	g_map = map_scene.instantiate()
	g_map.position=Vector2(294,18)
	add_child(g_map)
	
	g_level = level_scene.instantiate()
	g_level.connect("update", g_map._on_level_update)
	g_level.connect("on_end", on_level_end)
	g_level.g_width = 8
	g_level.g_height = 6
	add_child(g_level)
	
	if DisplayServer.is_touchscreen_available():
		g_keypad = keypad_scene.instantiate()
		add_child(g_keypad)
	

func on_level_end(_data):
	remove_child(g_gui)
	g_gui.queue_free()
	
	remove_child(g_level)
	g_level.queue_free()
	
	remove_child(g_map)
	g_map.queue_free()
	
	if g_keypad!=null:
		remove_child(g_keypad)
		g_keypad.queue_free()
	
	add_child(g_start)
	g_start.init()
	
