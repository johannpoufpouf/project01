extends Area2D

@onready var g_animationtree = $animation/tree
@onready var g_state = g_animationtree["parameters/playback"]

#========= ATTRIBUTES ==========================================================
var g_is_alive			= false
var g_direction			= Vector2(1,0)	: set = set_direction
var g_speed				= 250			: set = set_speed
var g_initial_speed		= Vector2(0,0)	: set = set_initial_speed
var g_initial_height	= 10 			: set = set_initial_height
var g_distance			= 100			: set = set_distance, get = get_distance
var g_weight			= 1				: set = set_weight
var g_force:float		= 1				: set = set_force
var g_foe				= false

#========= ACCESSORS ===========================================================
func set_direction(_dir)			: g_direction = _dir
func set_initial_height(_h)			: g_initial_height = _h
func set_distance(_d)				: g_distance = _d
func get_distance()					: return g_distance
func set_alive()					: g_is_alive = true
func set_weight(_w)					: g_weight = _w
func set_initial_speed(_v : Vector2): g_initial_speed = _v ; upd_distance()
func set_speed(_s: int)				: g_speed = _s ; upd_distance()
func set_force(_f: float)			: g_force = _f 
func set_foe()						:
	g_foe = true
	# scale=Vector2(1.5,1.5)
	set_collision_mask_value(10, false)
	set_collision_mask_value(14, false)
	set_collision_mask_value(9, true)
	set_collision_mask_value(13, true)
func upd_distance():
	g_distance = g_distance * (g_speed + g_direction.dot(g_initial_speed)) / g_speed

#========= VARIABLES ===========================================================
var m_height			= 0				# BULLET CURRENT HEIGHT
var m_distance			= 0				# BULLET DISTANCE FROM EMISSION POINT
var m_hit				= null			# HIT INSTANCE
var m_move				= Vector2.ZERO	# LAST MOVE

#========= HANDLE BULLET HEIGHT: TOO CLOSE FROM GROUND, IT ENDS ================
func update_height() :
	$sprite/body.position.y=-m_height
	if (m_height<5) : end(0)
	
#========= INITIALISATION ======================================================
func _ready():
	m_height = g_initial_height ; update_height()
	m_hit = GLOBAL.Hit.new()
	m_hit.node = self ; m_hit.weight = g_weight ; m_hit.foe = g_foe ; m_hit.force = g_force

#========= MOVE BULLET =========================================================
func _process(delta):
	if g_is_alive:
		m_move = (g_direction*g_speed + g_initial_speed)*delta
		if m_distance < get_distance() :
			m_distance = m_distance + m_move.length()
		else :
			m_move.x = m_move.x/5
			m_height = (m_height-1)/2
			update_height()
		position = position + m_move

#========= HIT SOMETHING =======================================================
func _on_area_entered(area) :
	if area.owner.has_method("on_hit") : area.owner.on_hit(m_hit)
	# end((area.owner.position-position).angle() - PI/2)
	end(m_move.angle() - PI/2)

#========= BULLET END ==========================================================
func end(_angle) :
	g_is_alive = false
	$sprite/body.set_global_rotation(_angle)
	g_state.travel("end")

#========= DESTRUCTOR ==========================================================
func _on_tree_animation_finished(anim_name): if anim_name=="end" : queue_free()
