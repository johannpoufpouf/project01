extends CharacterBody2D

@export_enum("default","boss", "sewer") var type:String = "default"
@export_enum("right:0", "left:1", "up:2", "down:3") var dir:int = 0

signal door_pass(_dir, _type)

func _ready(): idle()

func set_dir(_dir:int)->void:		dir= _dir
func set_type(_type:String)->void:	type = _type

func idle()->void:	$animation/def.play(type+".idle."+GLOBAL.DIRSTR[dir])
func close()->void:	$animation/def.play(type+".close."+GLOBAL.DIRSTR[dir])
func open()->void:	$animation/def.play(type+".open."+GLOBAL.DIRSTR[dir])

func _on_hitbox_body_entered(_body): emit_signal("door_pass", dir, type)

