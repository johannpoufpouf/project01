extends StaticBody2D

var g_opened:bool = false
var g_fall:bool = false

func fall(): g_fall = true

func _ready():
	visible = false
	if g_fall:	$animation/def.play("fall")
	else:		$animation/def.play("intro")
	

#======== PLAYER TOUCH THE CLOSED BOX ==========================================
func _on_hitbox_body_entered(_body):
	if !g_opened:
		g_opened = true
		$animation/def.play("open")
		$sprite/glitters.emitting = true

#======== OPEN THE BONUS DIALOG ================================================
func on_open_end(): if GLOBAL.BONUS_SCENE: GLOBAL.BONUS_SCENE.init(true)

