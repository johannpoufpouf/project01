extends m_generic

@onready var g_animationtree = $animation/tree
@onready var g_state = g_animationtree["parameters/playback"]

@export var life: int = 8				# LIFE

#======== GENERIC CALLBACK =====================================================
func on_hit_cbk(_hit : GLOBAL.Hit):		g_state.travel("hit")
func on_die_cbk():						g_state.travel("die")
func on_end(): 							queue_free()

#======== ATTRIBUTES ===========================================================
var m_period		= 0					# CURRENT PERIOD
var m_angle			= 0					# RANDOM ANGLE FOR EACH PERIOD
var m_run			= false				# RUN WHEN DIE

#======== INITIALISATION =======================================================
func _ready():
	super()
	set_life_max(life) ; set_life(life)
	set_period(1.2); set_weight(0.8) ; set_speed(50) ; set_accel(50)
	m_hit.node = $hit

#======== MOVE: 0 STAY, 1-3 MOVE TO PLAYER, 4 MOVE RANDOMLY=====================
func _physics_process(_delta):
	if m_run:
		velocity = Vector2(1,0).rotated(m_angle)*get_speed()
		move_and_slide()
		return
		
	if !is_active() || is_hit(): return
	
	super(_delta)
	var v_target = Vector2.ZERO
	if is_alive() :
		if m_period<3 || m_period%3 :
			v_target = Vector2(1,0).rotated(m_angle)
			velocity = velocity.move_toward(v_target*get_speed(), get_accel()*_delta)
		else :
			velocity = Vector2.ZERO
			g_state.travel("obey")
		move_and_slide()

#======== ON PERIOD CALLBACK ===================================================
func on_period(_period)	:
	if is_alive():
		m_period = _period
		m_angle = 2*PI*GLOBAL.RNG.randi_range(0,4)/4

#======== ON HIT ===============================================================
func _on_hit_area_entered(area): super(area) ; on_die()

func set_run_on(): m_run = true
func set_run_off(): m_run = false

func freeze(): if GLOBAL.PLAYER!=null: GLOBAL.PLAYER.freeze()

