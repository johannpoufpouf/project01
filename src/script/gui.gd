extends Node2D

var g_time:float = 0
var g_last_milli: int = 0

#========= INITIALISATION ======================================================
func init():
	update_nb_bombs() ; update_hearts()
	g_time = 0 ; g_last_milli = 0

	# CONNECT TO PLAYER STATISTICS
	GLOBAL.PLAYER.update_stats.connect(_on_pingmy_update_stats)
	
func _physics_process(_delta):
	g_time = g_time + _delta
	if g_last_milli!= floori(g_time*10):
		g_last_milli = floori(g_time*10)
		
		var v_milli  = floori(g_time*10)%10
		var v_milli_str= str(v_milli)
		
		var v_second = floori(g_time)%60
		var v_second_str= str(v_second)
		if v_second<10: v_second_str="0"+v_second_str
		
		var v_minute = floori(g_time/60)%60
		var v_minute_str= str(v_minute)
		if v_minute<10: v_minute_str="0"+v_minute_str
		
		var v_hour = floori(g_time/3600)
		var v_hour_str= str(v_hour)
		if v_hour<10: v_hour_str="0"+v_hour_str
		
		GLOBAL.STAT.time= v_hour_str+":"+v_minute_str+":"+v_second_str+"."+v_milli_str
		$time.text = GLOBAL.STAT.time

#========= BOMB ================================================================
func update_nb_bombs(): $nb_bombs.text = str(GLOBAL.PLAYER.get_nb_bombs())

#========= HEARTS ==============================================================
func travel(_state, _value) :
	if (_state.get_current_node()!=_value) : _state.travel(_value)
	
func update_hearts():
	var v_id = 0
	for child in $hearts.get_children() :
		var v_animationtree = child.find_child("tree")
		var v_state			= v_animationtree["parameters/playback"]
		if v_id < GLOBAL.PLAYER.get_int_life() : travel(v_state, "rednew")
		elif v_id < GLOBAL.PLAYER.get_life_max() : travel(v_state, "heartnew")
		elif v_id < GLOBAL.PLAYER.get_life_max() + GLOBAL.PLAYER.get_int_life_ex() : travel(v_state, "yellownew")
		else: travel(v_state, "heartnone")
		v_id = v_id+1

#========= UPDATE STATISTICS ===================================================
func _on_pingmy_update_stats(_what, _data):
	match _what :
		"nb_bombs" :
			if $animation/ref.is_playing(): update_nb_bombs()
			else:							$animation/ref.play("nb_bombs")
		"life_ex", "life" : update_hearts()

