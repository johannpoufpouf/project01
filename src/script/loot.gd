extends CharacterBody2D

@export_enum("red","yellow", "bomb") var type:String = "red"

func _ready(): $def.play(type)

func _physics_process(_delta):
	velocity = velocity.move_toward(Vector2.ZERO, 300*_delta)
	
	var v_collision = move_and_collide(velocity * _delta)
	if v_collision:
		velocity = velocity.bounce(v_collision.get_normal())

func on_hit(_hit):
	velocity=(global_position-_hit.node.global_position).normalized()*250

func _on_hitbox_area_entered(area):
	if area.owner.get_loot(type):
		$def.play("end")
	else:
		velocity=(position-area.owner.position).normalized()*200
