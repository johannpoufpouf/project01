extends m_generic

@onready var g_animationtree = $animation/tree
@onready var g_state = g_animationtree["parameters/playback"]

@export var life: int = 8				# LIFE

#======== GENERIC CALLBACK =====================================================
func on_hit_cbk(_hit : GLOBAL.Hit):		g_state.travel("hit")
func on_die_cbk():						g_state.travel("die")
func on_end(): 							queue_free()

#======== ATTRIBUTES ===========================================================
var m_period		= 0					# CURRENT PERIOD
var m_stop_period	= 0					# STOP PERIOD
var m_angle			= 0					# RANDOM ANGLE FOR EACH PERIOD
const nb_period		= 5					# PERIOD CYCLE

#======== INITIALISATION =======================================================
func _ready():
	super()
	m_stop_period = GLOBAL.RNG.randi_range(0,nb_period-1)
	set_life_max(life) ; set_life(life)
	set_period(1); set_weight(0.8) ; set_speed(50) ; set_accel(50)
	m_hit.node = $hit

#======== MOVE: 0 STAY, 1-3 MOVE TO PLAYER, 4 MOVE RANDOMLY=====================
func _physics_process(_delta):
	if !is_active(): return
	super(_delta)
	var v_target = Vector2.ZERO
	var v_period = m_period % nb_period
	if is_alive() && (v_period!=0) :
		if v_period == m_stop_period || GLOBAL.PLAYER==null :
			v_target = Vector2(1,0).rotated(m_angle)
		else :
			v_target = (GLOBAL.PLAYER.position - position).normalized()
		g_animationtree.set("parameters/idle/blend_position", v_target.x)
		g_animationtree.set("parameters/die/blend_position", v_target.x)
		
	velocity = velocity.move_toward(v_target*get_speed(), get_accel()*_delta)
	move_and_slide()

#======== ON PERIOD CALLBACK ===================================================
func on_period(_period)	:
	if is_alive():
		m_period = _period
		m_angle = GLOBAL.RNG.randf_range(0,2*PI)

#======== ON HIT ===============================================================
func _on_hit_area_entered(area): super(area) ; on_die()
