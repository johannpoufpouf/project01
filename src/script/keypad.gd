extends Control

var g_touch_index: int = -1
var g_initpos = Vector2.ZERO
var g_pos = Vector2.ZERO

func _ready():
	visible = DisplayServer.is_touchscreen_available()
	g_initpos = $left/key.position
	
func _input(_event):
	if _event is InputEventScreenTouch:
		if _event.pressed:
			if  g_touch_index == -1 && is_point_inside_joystick_area(_event.position) :
				g_touch_index = _event.index
				g_pos = _event.position
				$left/key.region_rect= Rect2(304, 0, 16, 16)
				get_viewport().set_input_as_handled()	
		elif _event.index == g_touch_index:
			g_touch_index = -1
			g_pos = Vector2.ZERO
			$left/key.position = g_initpos
			Input.action_press("ui_right", 0)
			Input.action_press("ui_left", 0)
			Input.action_press("ui_up", 0)
			Input.action_press("ui_down", 0)
			$left/key.region_rect= Rect2(288, 0, 16, 16)
			get_viewport().set_input_as_handled()
	elif _event is InputEventScreenDrag:
		if _event.index == g_touch_index:
			var v_dif = _event.position - g_pos
			var v_len = v_dif.length()
			if v_len>20 : v_len=20
			v_dif = v_dif.normalized()
			
			if v_len<2 :
				Input.action_press("ui_right", 0)
				Input.action_press("ui_left", 0)
				Input.action_press("ui_up", 0)
				Input.action_press("ui_down", 0)
			else:
				if v_dif.x>0 : Input.action_press("ui_right", v_dif.x)
				else : Input.action_press("ui_left", -v_dif.x)
				if v_dif.y>0 : Input.action_press("ui_down", v_dif.y)
				else : Input.action_press("ui_up", -v_dif.y)
			$left/key.position = g_initpos + v_dif*v_len
			get_viewport().set_input_as_handled()

func is_point_inside_joystick_area(_pos) -> bool :
	return (_pos-$left/circle.global_position).length_squared()<400
