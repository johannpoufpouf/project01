extends CharacterBody2D

# HANDLE COLLISION: BOMB CREATED BY PLAYER GET COLLISION ONCE PLAYER LEFT AREA
func set_collision_on() :
	$collision_shape.disabled = false
	$hitbox/hitbox_shape.disabled = false
func set_collision_off():
	$collision_shape.disabled = true
	$hitbox/hitbox_shape.disabled = false

var m_hit:			GLOBAL.Hit	= null				# HIT OBJECT
var m_velocity:		int			= 50				# VELOCITY WHEN MOVED
var m_friction:		int			= 100				# FRICTION WHEN MOVED
var m_weight:		float		= 1.5					# BOMB WEIGHT

#======== INITIALISATION =======================================================
func _ready() :
	m_hit = GLOBAL.Hit.new()
	m_hit.node = $hit ; m_hit.force = 3 ; m_hit.weight = 5

#======== MOVE AFTER BE PUSHED =================================================
func _physics_process(_delta):
	velocity = velocity.move_toward(Vector2.ZERO, m_friction*_delta)
	if (velocity!=Vector2.ZERO) : move_and_slide()

#======== PLAYER LEFT THE HITBOX AREA ==========================================
func _on_hitbox_body_exited(body):
	if $collision_shape.disabled :
		call_deferred("set_collision_on")
		if body.has_method("can_bomb") : body.can_bomb()

#======== DESTRUCTOR ===========================================================
func on_end(): queue_free()

#======== EXPLOSION HIT SOMETHING ==============================================
func _on_hit_area_entered(area):
	if area.owner.has_method("on_hit") : area.owner.on_hit(m_hit)
	
#======== ON_HIT: HANDLE BOUNCE ================================================
func on_hit(_hit : GLOBAL.Hit) :
	if _hit.node != $hit :
		if _hit.weight && m_weight:
			var v_ratio: float = 1.0 * _hit.weight / m_weight
			var v_dir: Vector2 = global_position - _hit.node.global_position
			velocity = v_dir.normalized() * v_ratio * m_velocity

