extends Node

#========= DEFINES =============================================================
const ENEMY_GROUP=		"ENEMY"
const DOOR_GROUP=		"DOOR"
const CARD_GROUP=		"CARD"

#========= CURRENT PLAYER ======================================================
var PLAYER = null
func get_player():
	if PLAYER == null :
		var v_player_scene = load("res://src/scene/generic/player/pingmy.tscn")
		PLAYER = v_player_scene.instantiate()
	return PLAYER
	
func set_player(_player) :
	if PLAYER!=null && PLAYER!=_player: print("[ERR] set_player issue");
	PLAYER = _player

#========= DIRECTION DEFINITIONS ===============================================
enum DIR {BELOW=-2, NONE=-1, RIGHT=0, LEFT=1, UP=2, DOWN=3}
const DIRVEC	= [ Vector2(1,0), Vector2(-1,0), Vector2(0,-1), Vector2(0,1)]
const INVDIR	= [ DIR.LEFT, DIR.RIGHT, DIR.DOWN, DIR.UP ]
const DIRSTR	= [ "right", "left", "up", "down"]

#========= RANDOMIZER ==========================================================
var RNG = RandomNumberGenerator.new()
func _ready():
	randomize()
	RenderingServer.set_default_clear_color(Color.BLACK)

#========= GENERIC HIT CLASS ===================================================
class Hit:
	var node	: Node2D	= null			# EMITTER NODE
	var force	: int		= 1				# HIT FORCE
	var weight 	: int		= 1				# WEIGHT OF THE HITER
	var foe		: bool		= false			# FROM PLAYER OR FOE
	var freeze	: bool		= false
	
	func clean(): 			force = 1; weight = 1
	func get_weight():		return weight
	
#======== STATS ================================================================

var STAT_SCENE=		null

var STAT= {
	"dead"				: 0,
	"bullet"			: 0,
	"bomb"				: 0,
	"room"				: 0,
	"card"				: 0,
	"time"				: "00:00:00.0"
}

func clean_stat():
	STAT.dead=			0
	STAT.bullet=		0
	STAT.bomb=			0
	STAT.room=			0
	STAT.card=			0
	STAT.time=			"00:00:00.0"

#======== SKILLS ===============================================================

var BONUS_SCENE=	null

var MAX= {
	"luck"				: 10,
	"nb_fires"			: 4,
	"nb_back_fires"		: 2
}

var BONUS= {
	"fire_size"			: 1.0,
	"speed"				: 1.0,
	"bullet_speed"		: 1.0,
	"map_all_visible"	: false,
	"map_spe_visible"   : false
}

var MALUS= {
	"speed"				: 1.0,
	"fire_size"			: 1.0
}

func clean_bonus_malus():
	BONUS.fire_size			= 1.0
	BONUS.speed				= 1.0
	BONUS.bullet_speed		= 1.0
	BONUS.map_all_visible	= false
	BONUS.map_spe_visible   = false

	MALUS.speed				= 1.0
	MALUS.fire_size			= 1.0
	

