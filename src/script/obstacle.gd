extends m_generic

@export_category("Obstacle")
@export_enum("default", "stone", "garbage", "bomb", "spike") \
		var type: String = "default"
@export var index: int = 1
@export var force: int = 1

@export_category("Loot")
@export var red: int = 20
@export var yellow: int = 20
@export var bomb: int = 20

@onready var g_anim			= $animation/def
@onready var g_hitbox_shape	= $hitbox/hitbox_shape
func get_hitbox_shape() : return g_hitbox_shape

#======== ATTRIBUTES ===========================================================
var m_type							# OBSTACLE TYPE
var m_hit_by_bullet = true			# CAN BE HIT BY A BULLET


func _ready():
	super()
	set_static()
	m_hit.node = $hit

	m_type = type
	match type :
		"bomb":
			$hit.set_collision_layer_value(18,true)		# HIT BOMB LAYER
			$hit.set_collision_mask_value(14,true)		# HIT FLYING FOES TOO
			m_hit.weight = 3
			m_hit.force = 3
		"stone" :
			m_type=m_type+"0"+str(clamp(index,1,5))
			m_hit_by_bullet = false
		"garbage" : m_type=m_type+"0"+str(clamp(index,1,4))
		"spike" :
			if (index==1) : m_type=m_type+"_active_a"
			elif (index==2) : m_type=m_type+"_active_b"
			else : m_type=m_type+"_static"
			m_hit.weight = 0
	g_anim.play(m_type)

#======== HIT STUFF ON HIT AREA ================================================
func _on_hit_area_entered(area): super(area)

#======== UPDATE OBSTACLE LIFE =================================================
func handle_hit(_hit : GLOBAL.Hit) :
	if _hit.node.get_collision_layer_value(18) : g_data.life.value = 0
	else : g_data.life.value = g_data.life.value - 1

func on_hit_cbk(_hit : GLOBAL.Hit):		g_anim.play("hit")
func on_hit_end_cbk():					g_anim.play(m_type)
func on_die_cbk():						g_anim.play(m_type+".end")

func check_hit(_hit : GLOBAL.Hit):
	return !_hit.foe && (!_hit.node.get_collision_layer_value(17) || m_hit_by_bullet)
	
func compute_hit(_hit : GLOBAL.Hit) :
	var v_ret = 1
	if (_hit.node.get_collision_layer_value(18)) : v_ret = 5
	return v_ret

func on_end():
	$hit.queue_free()
	$hitbox.queue_free()
	$fx.queue_free()
	$parts.queue_free()
	
func on_loot():
	var v_loots = { "red":red, "yellow":yellow, "bomb":bomb}
	for v_type in v_loots :
		if GLOBAL.RNG.randi_range(0,v_loots[v_type])==0:
			var v_loot = loot_scene.instantiate()
			var v_dir = Vector2(1,0).rotated(GLOBAL.RNG.randf_range(0,2*PI))
			v_loot.type= v_type
			v_loot.position = position+v_dir*10
			v_loot.velocity = v_dir*50 
			get_parent().add_child(v_loot)
			
