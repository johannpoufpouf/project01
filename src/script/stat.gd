extends Node2D

var g_available = false
var g_timeout: float = 1
func set_available(): g_available=true

const g_frames = {
	"anar":	274, "peace":275, "anonymous":276, "yellow":277,
	"crap": 278, "googles": 279, "hook": 280, "music": 281,
	"speed_up": 282, "beer": 283, "tag": 284,
	"revolution":306, "stone": 307, "molotov":308, "megaphone": 309,
	"sign": 310, "bbq": 311, "garbage": 312, "speed_down": 313
}

#======== INITIALISATION =======================================================
func _ready():
	GLOBAL.STAT_SCENE=self
	visible=false
	$fullbutton.visible = !DisplayServer.is_touchscreen_available()

#======== UPDATE AND OPEN STAT PANEL ===========================================
func open():
	$values/dead.text= str(GLOBAL.STAT.dead)
	$values/bullet.text= str(GLOBAL.STAT.bullet)
	$values/bomb.text= str(GLOBAL.STAT.bomb)
	$values/room.text= str(GLOBAL.STAT.room)
	$values/time.text = GLOBAL.STAT.time
	
	$values/speed.text = str(roundf(GLOBAL.BONUS.speed*10)/10)
	
	if GLOBAL.PLAYER:
		$values/hearts.text = str(GLOBAL.PLAYER.get_life())
		$values/force.text = str(roundf(GLOBAL.PLAYER.get_force()*100)/100)
		$values/armor.text = str(GLOBAL.PLAYER.get_armor())+"%"
		$values/luck.text = str(GLOBAL.PLAYER.get_luck())
		$values/front.text = str(GLOBAL.PLAYER.get_nb_fires())
		$values/back.text = str(GLOBAL.PLAYER.get_nb_back_fires())
		
		#=== SHOW BONUS/MALUS LIST ===
		var v_x = 0
		var v_y = 0
		for elt in $bonus.get_children(): elt.queue_free()
		for i in GLOBAL.PLAYER.m_bonus_list:
			var v_bonus = $animation/template_bonus.duplicate()
			if g_frames.has(i) : v_bonus.frame = g_frames[i]
			else: v_bonus.frame=273
			v_bonus.position = Vector2i(v_x, v_y)
			v_bonus.visible = true
			v_bonus.name = "bonus_"+i
			$bonus.add_child(v_bonus)
			
			if GLOBAL.PLAYER.m_bonus_list[i]>1 :
				var v_label = v_bonus.get_child(0)
				v_label.text=str(GLOBAL.PLAYER.m_bonus_list[i])
				v_label.visible = true
			v_x = v_x + 18 ; if v_x>90 : v_x = 0 ; v_y = v_y + 18
			
			
	get_tree().paused = true
	g_available = false
	$animation/def.play("open")
	

func _process(_delta):
	if g_available:
		if g_timeout>0 : g_timeout = g_timeout - _delta
		if Input.is_action_pressed("ui_cancel"):
			g_available=false
			get_tree().paused = false
			$animation/def.play("close")
		elif Input.is_action_pressed("ui_bomb"):
			if g_timeout<=0:
				g_timeout = 0.3
				if DisplayServer.window_get_mode() != DisplayServer.WINDOW_MODE_FULLSCREEN:
					DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
				else:
					DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
