extends m_generic
var bomb_scene	= preload("res://src/scene/bomb.tscn")

signal pingmy_position(_pos)
signal pingmy_die()
signal pingmy_end()

@onready var g_animationtree	= $animation/tree
@onready var g_state			= g_animationtree["parameters/playback"]
@onready var g_hitbox			= $hitbox

func get_hitbox() : return g_hitbox

#======== PLAYER SPECIFICS ATTRIBUTE ===========================================
var g_dir_id:int				= GLOBAL.DIR.RIGHT
var m_can_bomb:bool				= true
var m_bonus_list				= {}

var m_data = {
	"fire"			: { "rate": 3, "max": 5, "available": true },
	"life_ex"		: 0,				# YELLOW HEARTS
	"nb_bombs"		: 3,				# BOMBS
	"luck"			: 0,				# LUCK
	"nb_fires"		: 1,				# FIRE NUMBER
	"nb_back_fires"	: 0					# BACK FIRE NUMBER
}

func dump():
	super()
	print("  + bomb:   "+str(m_can_bomb))
	print("  + dir:    "+str(g_dir_id))
	print("  + fire:   "+str(m_data.fire.available))
	print("  + luck:   "+str(m_data.luck))

func travel(_value): if (g_state.get_current_node()!=_value): g_state.travel(_value)

func inc_luck(_value):	set_luck(m_data.luck+_value)
func set_luck(_value):	m_data.luck=clampi(_value,0,GLOBAL.MAX.luck)
func get_luck()->int:	return m_data.luck

func inc_nb_fires(_value):	set_nb_fires(m_data.nb_fires+_value)
func set_nb_fires(_value):	m_data.nb_fires=clampi(_value,1,GLOBAL.MAX.nb_fires)
func get_nb_fires()->int:	return m_data.nb_fires

func inc_nb_back_fires(_value):	set_nb_back_fires(m_data.nb_back_fires+_value)
func set_nb_back_fires(_value):	m_data.nb_back_fires=clampi(_value,0,GLOBAL.MAX.nb_back_fires)
func get_nb_back_fires()->int:	return m_data.nb_back_fires

#========= LIFE ================================================================
const g_nb_max_hearts = 10
func set_life_max(_value):
	g_data.life.max = clamp(_value,0,get_life_max_max())
	emit_signal("update_stats", "life_max", get_life_max())
func get_life_max_max():		return g_nb_max_hearts-get_life_ex()
func inc_life_max(_value):		set_life_max(get_life_max()+_value) ; inc_life(_value)
func dec_life_max(_value):		set_life_max(get_life_max()-_value) ; upd_life()

func set_life_ex(_value:float):
	m_data.life_ex=clamp(_value,0,g_nb_max_hearts-get_life_max())
	emit_signal("update_stats", "life_ex", get_int_life_ex())
func get_life_ex()->float:		return m_data.life_ex
func get_int_life_ex()->int:	return roundi(get_life_ex())
func inc_life_ex(_value:float):	set_life_ex(get_life_ex()+_value)
func dec_life_ex(_value:float):	set_life_ex(get_life_ex()-_value)

func get_all_life()->float:		return get_life()+get_life_ex()
func get_all_int_life()->int:	return get_int_life()+get_int_life_ex()
func is_alive():				return get_all_int_life()>0			# OVERWRITE

#========= BOMB ================================================================
func get_nb_bombs()	: return m_data.nb_bombs
func dec_nb_bombs()	:
	if m_data.nb_bombs>0 :
		m_data.nb_bombs = m_data.nb_bombs - 1
		emit_signal("update_stats","nb_bombs", m_data.nb_bombs)
func set_nb_bombs(_value):
	m_data.nb_bombs = clamp(_value,0,99)
	emit_signal("update_stats","nb_bombs", m_data.nb_bombs)
func inc_nb_bombs(_value): set_nb_bombs(get_nb_bombs()+_value)

#======== INITIALISATION =======================================================
func _ready():
	super()
	m_bonus_list.clear()
	GLOBAL.set_player(self)		# PLAYER NODE IS AVAILABLE FROM GLOBAL
	set_not_foe() ; set_inactive() ; visible = false
	set_life(3); set_life_max(3); set_life_ex(0); set_force(3)
	set_speed(100); set_accel(100); set_friction(5); set_step(1000)

#======== SHOOT FIRE ===========================================================
func fire(_shoot_id, _offset, _scale):
	var v_bullet	= bullet_scene.instantiate()
	var v_position	= position + $content/sprite/beak/origin.global_position - global_position + _offset
	var v_height	= (position.y-v_position.y+ _offset.y)/GLOBAL.BONUS.fire_size
	v_position.y	= v_position.y + v_height + GLOBAL.DIRVEC[_shoot_id].y*4
	v_bullet.set_initial_height(v_height)
	v_bullet.set_position(v_position)
	v_bullet.set_direction(GLOBAL.DIRVEC[_shoot_id])
	v_bullet.set_initial_speed(velocity)
	v_bullet.set_speed(250*GLOBAL.BONUS.bullet_speed)
	v_bullet.set_force(get_force())
	if _scale!=1: v_bullet.scale = Vector2(_scale,_scale)
	GLOBAL.STAT.bullet = GLOBAL.STAT.bullet + 1
	get_parent().add_child(v_bullet)
				
#======== PLAYER INPUTS ========================================================
func _physics_process(_delta):
	if !is_active(): return
	super(_delta)
	var v_input_dir	= Vector2(Input.get_axis("ui_left", "ui_right"), \
						Input.get_axis("ui_up", "ui_down")).normalized()
	var v_shoot_id	= -1
	
	# STAT
	if Input.is_action_just_pressed("ui_cancel") && GLOBAL.STAT_SCENE: GLOBAL.STAT_SCENE.open()
	
	# BOMB
	if m_can_bomb && Input.is_action_pressed("ui_bomb") && get_nb_bombs()>0 :
		m_can_bomb = false
		var v_bomb = bomb_scene.instantiate()
		v_bomb.set_position(position)
		v_bomb.set_collision_off()
		get_parent().add_child(v_bomb)
		GLOBAL.STAT.bomb=GLOBAL.STAT.bomb+1
		dec_nb_bombs()
	
	# SHOOT
	if m_can_bomb && !is_hit() :
		for i in 4: if Input.is_action_pressed("ui_fire_"+GLOBAL.DIRSTR[i]) : v_shoot_id = i
		if v_shoot_id != -1 :
			set_direction(GLOBAL.DIRVEC[v_shoot_id])
			
			# PINGUIN NEED TO BE IN THE GOOD DIRECTION BEFORE SHOOTING
			# OTHERWISE THE BEAK ORIGIN WILL NOT BE IN THE GOOD PLACE
			if v_shoot_id == g_dir_id && m_data.fire.available :
				for i in get_nb_fires():
					fire(v_shoot_id,
						GLOBAL.BONUS.fire_size*10.0*(i-1.0*(get_nb_fires()-1)/2)*GLOBAL.DIRVEC[(v_shoot_id+2)%4],
						GLOBAL.BONUS.fire_size)
				for i in get_nb_back_fires():
					fire(GLOBAL.INVDIR[v_shoot_id],
						10*GLOBAL.DIRVEC[GLOBAL.INVDIR[v_shoot_id]]+GLOBAL.BONUS.fire_size*10.0*(i-1.0*(get_nb_back_fires()-1)/2)*GLOBAL.DIRVEC[(GLOBAL.INVDIR[v_shoot_id]+2)%4],
						GLOBAL.BONUS.fire_size*0.8)
				
				$content/sprite/beak.visible = true
				$content/sprite/fx_fire.start()
				
				# FIRE FREQUENCY
				m_data.fire.available = false
				$animation/fire.wait_time=1-m_data.fire.rate*(0.8/m_data.fire.max)
				$animation/fire.start()
	
	# MOVE
	var v_target = Vector2.ZERO
	var v_accel = get_friction()*get_accel()*3
	if v_input_dir!=Vector2.ZERO :
		v_target = get_speed() * v_input_dir
		if v_input_dir.dot(velocity) > 0:	v_accel = get_friction()*get_accel()
		if v_shoot_id == -1:				set_direction(v_input_dir)
		if !is_hit():						travel("run")
	else :
		if !is_hit():						travel("idle")

	
	velocity = velocity.move_toward(v_target, v_accel*_delta)
	# var v_collision = move_and_collide(velocity*_delta)
	if (velocity!=Vector2.ZERO) : move_and_slide()
	
	emit_signal("pingmy_position",position)

#======== PLAYER STATE CALLBACK ================================================
func go_right():					g_dir_id = GLOBAL.DIR.RIGHT
func go_left():						g_dir_id = GLOBAL.DIR.LEFT
func go_up():						g_dir_id = GLOBAL.DIR.UP
func go_down():						g_dir_id = GLOBAL.DIR.DOWN
func _on_fire_timeout():			m_data.fire.available = true
func _on_fx_fire_on_finished():		$content/sprite/beak.visible = false
func can_bomb():					m_can_bomb = true

func set_direction(_dir):
	g_animationtree.set("parameters/run/blend_position", _dir)
	g_animationtree.set("parameters/idle/blend_position", _dir)
	g_animationtree.set("parameters/in/blend_position", _dir)
	g_animationtree.set("parameters/out/blend_position", _dir)

#======== PLAYER HAS DONE A WHOLE STEP =========================================
func on_step() :
	var v_dust = fx_scene.instantiate()
	v_dust.set_type("dust",0)
	v_dust.set_position(position)
	get_parent().add_child(v_dust)

#======== PLAYER HAS BEEN HIT ==================================================
func on_hit_cbk(_hit : GLOBAL.Hit) :
	$content/sprite/fx_hit.start()
	g_animationtree.set("parameters/hit/blend_position",
		(_hit.node.position-position).normalized())
	travel("hit")
	
func apply_hit(_value):
	print("[PLA] HIT: "+str(_value))
	var v_value = _value
	if get_life_ex() :
		if (get_life_ex()>=_value) : dec_life_ex(v_value) ; v_value = 0
		else : _value = _value - get_life_ex() ; set_life_ex(0)
	if v_value: dec_life(_value)

#======== PLAYER IS DEAD =======================================================
func on_die():
	set_inactive()
	emit_signal("pingmy_die")
	$content/sprite/fx_hit.start()
	travel("die")

func on_end(): emit_signal("pingmy_end")

func freeze():		velocity=Vector2.ZERO ; set_direction(GLOBAL.DIRVEC[GLOBAL.DIR.DOWN]) ; travel("frozen")
func set_active():	super() ; Input.flush_buffered_events()


func get_loot(_type)->bool:
	var v_ret: bool = false
	match _type:
		"red":
			v_ret = get_life()<get_life_max()
			if v_ret: inc_life(1)
		"yellow":
			v_ret = get_life_max()<get_life_max_max()
			if v_ret: inc_life_ex(1)
		"bomb":
			v_ret = get_nb_bombs()<99
			if v_ret: inc_nb_bombs(1)
	if v_ret: $stars.emitting=true
	return v_ret
	
func on_new_bonus(_type)->void:
	print("[DBG] On new bonus: "+_type)
	if m_bonus_list.has(_type):		m_bonus_list[_type]=m_bonus_list[_type]+1
	else:							m_bonus_list[_type]=1
	match _type:
		"anar":			GLOBAL.BONUS.fire_size=clampf(GLOBAL.BONUS.fire_size*1.1,1,2); mult_force(1.1)
		"anonymous":	inc_nb_back_fires(1)
		"bbq":			inc_life_ex(2) ; inc_luck(1)
		"beer":			inc_luck(2)
		"crap":			GLOBAL.MALUS.fire_size=clampf(GLOBAL.MALUS.fire_size*1.25,1,2);
		"hook":			mult_force(1.5)
		"molotov":		inc_nb_bombs(20) ; mult_force(1.1)
		"peace":		inc_life_max(1) ; inc_luck(1)
		"revolution":	inc_nb_bombs(5) ; mult_force(1.2)
		"sign":			inc_life_ex(1) ; inc_armor(10)
		"speed_down":	GLOBAL.BONUS.speed = clampf(GLOBAL.BONUS.speed * 0.9,0.5,2)
		"speed_up":		GLOBAL.BONUS.speed = clampf(GLOBAL.BONUS.speed * 1.2,0.5,2)
		"tag":			inc_nb_fires(1)
		"yellow":		inc_armor(20)

