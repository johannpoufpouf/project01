extends Node2D

var map_room_scene 		= preload("res://src/scene/ui/map_room.tscn")

var g_size:Array = [1,1]
var g_visible_size:Array = [1,1]
var g_offset:Array = [0,0]
var g_rooms:Array = []

func _ready(): pass

func _on_level_update(_type, _value, _option):
	match(_type):
		"clear":
			for room in g_rooms: if room!=null: room.queue_free()
			g_rooms.clear()
		"size":		g_size=[_value[0], _value[1]]
		"maze":
			g_rooms.clear()
			var v_id = 0
			for i in _value:
				if i==0: g_rooms.push_back(null)
				else:
					var v_room = map_room_scene.instantiate()
					match i:
						2:		v_room.get_node("def").play("preview.boss")
						5:		v_room.get_node("def").play("preview.treasure")
						_:		v_room.get_node("def").play("preview.default")
					v_room.position=Vector2(v_id%g_size[0]*7+3.5, floori((1.0*v_id/g_size[0]))*5+2.5)
					var v_visible = GLOBAL.BONUS.map_all_visible
					if i==2 && GLOBAL.BONUS.map_spe_visible : v_visible = true
					v_room.visible = v_visible
					$container/rooms.add_child(v_room)
					g_rooms.push_back(v_room)
				v_id = v_id+1
		"upd_room":
			var v_room = g_rooms[_value]
			if v_room:
				v_room.get_node("def").play(_option)
				v_room.visible = true
			
			if _option == "current":
				var v_pos=[_value%g_size[0], floori(1.0*_value/g_size[0])]
				var v_x:int = clampi(20 - v_pos[0]*7, - clampi((g_size[0]-6)*7,0,100), 0)
				var v_y:int = clampi(14 - v_pos[1]*5, - clampi((g_size[1]-5)*5,0,100), 0)
				$container/rooms.position = Vector2(v_x, v_y)
				
				if v_pos[0]>0 && g_rooms[_value-1]!=null: g_rooms[_value-1].visible = true
				if v_pos[0]<g_size[0]-1 && g_rooms[_value+1]!=null : g_rooms[_value+1].visible = true
				if v_pos[1]>0 && g_rooms[_value-g_size[0]]!=null: g_rooms[_value-g_size[0]].visible = true
				if v_pos[1]<g_size[1]-1 && g_rooms[_value+g_size[0]]!=null: g_rooms[_value+g_size[0]].visible = true
