extends Area2D

@export_category("Backdrop")
@export_enum("blob01", "mine01", "dust01", "dust02", "dust03", "dust04") \
		var type: String = "blob01"

@onready var g_anim			= $animation/def

#========= VARIABLES ===========================================================
var m_hit				= null			# HIT INSTANCE
var m_type								# BACKDROP TYPE

#======== INITIALISATION =======================================================
func _ready():
	m_hit = GLOBAL.Hit.new() ; m_hit.node = self ; m_hit.weight = 2
	m_type = type ; g_anim.play(m_type)

#======== DESTRUCTOR ===========================================================
func on_end(): queue_free()


func _on_body_entered(_body): g_anim.play(m_type+".end")

func _on_hit_area_entered(area):
	if area.owner.has_method("on_hit") : area.owner.on_hit(m_hit)
