extends Node2D

var room_scene	= preload("res://src/scene/room.tscn")

signal on_end(_recap)
signal update(_type,_value,_option)

#========= ATTRIBUTES ==========================================================
@export var g_width: int			= 9				# LEVEL WIDTH
@export var g_height: int			= 7				# LEVEL HEIGHT
@export var g_debug:bool			= false			# DEBUG LOGS
var g_maze: Maze					= null			# LEVEL MAZE
var g_empty_room: Room				= Room.new()	# EMPTY ROOM
var g_from_val: int					= 9				# TODO: FIND A WAY TO CREATE ENUM CONST
var g_current_room_id:int			= 0				# CURRENT ROOM ID
var g_next_room_id: int				= 0				# NEXT ROOM ID

#========= DEFINES =============================================================
const ROOM_EMPTY					= 0
const ROOM_DEFAULT					= 1
const ROOM_BOSS						= 2
const ROOM_LAST						= 3
const ROOM_START					= 4
const ROOM_TREASURE					= 5
const ROOM_ID_OFFSET				= 10

const ROOM_NAMES = [ "EMPTY", "DEFAULT", "BOSS", "LAST", "START", "TREASURE"]

#========= ROOM GLOSSARY =======================================================
# (RIGHT=8) (LEFT=4) (UP=2) (DOWN=1)
const ROOM_DEFS = {
	11:		[ 15, 1, ROOM_DEFAULT ],	12:		[ 15, 1, ROOM_DEFAULT ],
	13:		[ 15, 1, ROOM_DEFAULT ],	14:		[ 15, 1, ROOM_DEFAULT ],
	15:		[ 15, 1, ROOM_DEFAULT ],	16:		[ 15, 1, ROOM_DEFAULT ],
	17:		[ 15, 1, ROOM_DEFAULT ],	18:		[ 15, 1, ROOM_DEFAULT ],
	19:		[ 15, 1, ROOM_DEFAULT ],	20:		[ 15, 1, ROOM_DEFAULT ],
	21:		[ 15, 1, ROOM_DEFAULT],		22:		[ 15, 1, ROOM_DEFAULT],
	23:		[ 15, 1, ROOM_DEFAULT],
	51:		[ 13, 1, ROOM_DEFAULT ],	52:		[ 11, 1, ROOM_DEFAULT ],
	53:		[ 7, 1, ROOM_DEFAULT ],		54:		[ 14, 1, ROOM_DEFAULT ],
	101:	[ 7, 1, ROOM_DEFAULT ],		102:	[ 13, 1, ROOM_DEFAULT ],
	103:	[ 14, 1, ROOM_DEFAULT ],
	105:	[ 7, 1, ROOM_DEFAULT ],
	111:	[ 12, 1, ROOM_DEFAULT ],	112:	[ 3, 1, ROOM_DEFAULT],
	113:	[ 10, 1, ROOM_DEFAULT],		114:	[ 9, 1, ROOM_DEFAULT],
	115:	[ 5, 1, ROOM_DEFAULT],		116:	[ 6, 1, ROOM_DEFAULT],
	117:	[ 12, 1, ROOM_DEFAULT ],
	151:	[ 15, 1, ROOM_BOSS],
	201:	[ 15, 1, ROOM_TREASURE]
}

#========= LEVEL MAZE ==========================================================
class Maze:
	var maze: Array					= []
	var maze_id: Array				= []
	var rooms						= {}
	var width: int					= 0
	var height: int					= 0
	var boss_id: int				= -1
	var last_room_id: int			= -1
	var start_id: int				= -1

	#=== INIT MAZE WITH EMPY EDGES ===
	func init(_w:int, _h:int)->void:
		width = _w ; height = _h
		for y in height :
			for x in width :
				maze_id.push_back(ROOM_EMPTY)
				if x==0 || x==width-1 || y==0 || y == height-1 :
					maze.push_back(ROOM_EMPTY)
				else:
					maze.push_back(ROOM_DEFAULT)

	#=== REMOVE ROOMS TO BUILD MAZE ===
	func build(_ratio:int)->void:
		var v_clear=		0
		var v_attempt=		0
		var v_goal: int =	int(float(width*height)/_ratio)
		while v_clear<v_goal && v_attempt<10:
			var v_id = get_random_id()
			set_from_id(v_id, ROOM_EMPTY)
			if check():
				v_clear = v_clear + 1
				v_attempt = 0
			else:
				set_from_id(v_id, ROOM_DEFAULT)
				v_attempt = v_attempt+1
	
	#=== ACCESSORS AND CONVERSIONS ===
	# POS is a 2D array from [0,0] to [width-1,height-1]
	# ID is an index from 0 to width*height-1
	func pos_to_id(_pos: Array)->int:
		return _pos[0]+_pos[1]*width
		
	func get_from_id(_id: int)->int:
		var v_ret = ROOM_EMPTY
		if _id>=0 && _id<width*height : v_ret = maze[_id]
		return v_ret
		
	func set_from_id(_id: int, _val: int)->void:
		if _id>=0 && _id<width*height : maze[_id] = _val

	func get_from_pos(_pos: Array)->int :
		var v_ret = 0
		if _pos[0]>=0 && _pos[0]<width && _pos[1]>=0 && _pos[1]<height :
			v_ret = get_from_id(pos_to_id(_pos))
		return v_ret

	func set_from_pos(_pos: Array, _val: int)->void :
		if _pos[0]>=0 && _pos[0]<width && _pos[1]>=0 && _pos[1]<height :
			set_from_id(_pos[0]+_pos[1]*width, _val)

	func get_free_id_with_neighbor():
		while true:
			var v_id:int = GLOBAL.RNG.randi_range(0, width*height-1)
			if get_from_id(v_id)==ROOM_EMPTY && has_default_neighbor(v_id):
				return v_id
			
	func get_random_id() :
		while true:
			var v_id:int = GLOBAL.RNG.randi_range(0, width*height-1)
			if get_from_id(v_id)!=ROOM_EMPTY : return v_id

	func is_empty()->bool:
		var v_ret = true
		for v_val in maze: if v_val!= ROOM_EMPTY: v_ret = false
		return v_ret

	func dump()->void:
		print("Maze (width: "+str(width)+") (height: "+str(height)+")")
		var v_vals=[' ','X','B','L','S','T']
		for y in height:
			var v_line = ""
			for x in width:
				var v_val = get_from_pos([x,y])
				if v_val<ROOM_ID_OFFSET:	v_line = v_line+str(v_vals[v_val])
				else:						v_line = v_line+char(97+v_val-ROOM_ID_OFFSET)
			v_line=v_line+" "
			for x in width:
				var v_val = maze_id[pos_to_id([x,y])]
				if v_val<ROOM_ID_OFFSET:	v_line = v_line+str(v_vals[v_val])
				else:						v_line = v_line+char(97+v_val-ROOM_ID_OFFSET)
			print(v_line)
		for v_id in rooms: print("Room#"+char(97+v_id)+"|"+str(v_id)+" "+rooms[v_id].dump())
		print("start_id: "+str(start_id)+" - boss_id: "+str(boss_id))

	func get_neighbors(_id: int)->Array:
		var v_ret = []
		for i in 4 :
			var v_id = _id+GLOBAL.DIRVEC[i][0]+GLOBAL.DIRVEC[i][1]*width
			var v_val= get_from_id(v_id)
			if get_from_id(_id)==ROOM_BOSS || v_val==ROOM_BOSS :
				if get_from_id(_id)!=ROOM_LAST && v_val!=ROOM_LAST :
					v_val = ROOM_EMPTY
			v_ret.push_back([v_id, v_val])
		return v_ret
		
	func has_default_neighbor(_id:int)->bool:
		for i in 4 :
			var v_id = _id+GLOBAL.DIRVEC[i][0]+GLOBAL.DIRVEC[i][1]*width
			var v_val= get_from_id(v_id)
			if v_val==ROOM_DEFAULT || v_val==ROOM_LAST || v_val==ROOM_START: return true
		return false

	func add_boss()->void:
		while boss_id==-1:
			var v_id= get_random_id()
			var v_neighbors = get_neighbors(v_id)
			for i in 4 :
				if v_neighbors[i][1]==ROOM_EMPTY:
					boss_id=			v_neighbors[i][0]
					last_room_id=		v_id
		set_from_id(boss_id, ROOM_BOSS)
		set_from_id(last_room_id, ROOM_LAST)
		
	func add_treasure()->void:
		var v_id= get_free_id_with_neighbor()
		set_from_id(v_id, ROOM_TREASURE)

	func check()->bool:
		var v_maze_sav = maze.duplicate()
		var v_ids = [get_random_id()]
		while !v_ids.is_empty():
			var v_id= v_ids.pop_front()
			if maze[v_id]!=ROOM_EMPTY:
				var v_neighbors = get_neighbors(v_id)
				for i in 4 :
					if v_neighbors[i][1]!=ROOM_EMPTY : v_ids.push_back(v_neighbors[i][0])
				set_from_id(v_id, ROOM_EMPTY)
		var v_ret = is_empty()
		maze = v_maze_sav
		return v_ret

	func add_start()->void:
		const START = 9999
		var v_maze_sav = maze.duplicate()
		set_from_id(boss_id, START)
		set_from_id(last_room_id, START-1)
		var v_ids=[last_room_id]
		while !v_ids.is_empty():
			var v_id= v_ids.pop_front()
			var v_val= get_from_id(v_id)-1
			var v_neighbors= get_neighbors(v_id)
			for i in 4:
				if v_neighbors[i][1]!=ROOM_EMPTY && v_neighbors[i][1]<v_val:
					set_from_id(v_neighbors[i][0],v_val)
					v_ids.push_back(v_neighbors[i][0])
		var v_max_room_val= START
		for i in width*height:
			var v_val = get_from_id(i)
			if v_val!=ROOM_EMPTY && v_val<v_max_room_val:
				v_max_room_val = v_val
				start_id = i
		maze = v_maze_sav
		set_from_id(start_id, ROOM_START)
	
	func create_rooms():
		maze_id = maze.duplicate()
		
		if true:
			var v_x: int = 0
			var v_y: int = 0
			var v_id = 0
			while v_y<height:
				var v_val= maze_id[pos_to_id([v_x,v_y])]
				if v_val!=ROOM_EMPTY && v_val<ROOM_ID_OFFSET :
					var v_room=		Room.new()
					v_room.width=	1
					v_room.height=  1
					v_room.type=	v_val
					v_room.id=		pos_to_id([v_x, v_y])
					rooms[v_id]=	v_room
					
					for v_j in v_room.height : for v_i in v_room.width :
						maze_id[pos_to_id([v_x+v_i,v_y+v_j])]= v_id+ROOM_ID_OFFSET
					v_id = v_id+1
					v_x = v_x+v_room.width
				else: v_x = v_x+1
				if v_x>=width : v_x = 0 ; v_y = v_y + 1 
			
		# COMPUTE NEIGHBORS AND CHOOSE ROOM
		var v_already = []
		v_already.resize(ROOM_DEFS.size())
		v_already.fill(0)
		for v_id in rooms:
			var v_room = rooms[v_id]
			for v_i in v_room.width:
				var v_up= get_neighbors(v_room.id+v_i)[GLOBAL.DIR.UP]
				var v_up_val = -1
				if v_up[1]!=ROOM_EMPTY:	v_up_val= maze_id[v_up[0]]-ROOM_ID_OFFSET
				v_room.neighbors[GLOBAL.DIR.UP].push_back(v_up_val)
				
				var v_down= get_neighbors(v_room.id+v_i+(v_room.height-1)*width)[GLOBAL.DIR.DOWN]
				var v_down_val = -1
				if v_down[1]!=ROOM_EMPTY: v_down_val= maze_id[v_down[0]]-ROOM_ID_OFFSET
				v_room.neighbors[GLOBAL.DIR.DOWN].push_back(v_down_val)
					
			for v_j in v_room.height:
				var v_left= get_neighbors(v_room.id+v_j*width)[GLOBAL.DIR.LEFT]
				var v_left_val = -1
				if v_left[1]!=ROOM_EMPTY: v_left_val= maze_id[v_left[0]]-ROOM_ID_OFFSET
				v_room.neighbors[GLOBAL.DIR.LEFT].push_back(v_left_val)
				
				var v_right= get_neighbors(v_room.id+(v_room.width-1)+v_j*width)[GLOBAL.DIR.RIGHT]
				var v_right_val = -1
				if v_right[1]!=ROOM_EMPTY: v_right_val= maze_id[v_right[0]]-ROOM_ID_OFFSET
				v_room.neighbors[GLOBAL.DIR.RIGHT].push_back(v_right_val)
				
			if v_room.type != ROOM_START :
				
				var v_val = 0
				for v_i in 4 :
					v_val = v_val<<1
					if v_room.neighbors[v_i][0]!=-1 : v_val = v_val+1
					
				var v_available_rooms=[]
				var v_index = 0
				for v_i in ROOM_DEFS:
					if ROOM_DEFS[v_i][0]&v_val == v_val:
						var v_weight:float = 1
						if ROOM_DEFS[v_i][0]==v_val: v_weight=v_weight*5
						if v_already[v_index]==0: v_weight=v_weight*2
						var v_type = v_room.type
						if v_type == ROOM_LAST: v_type = ROOM_DEFAULT
						if ROOM_DEFS[v_i][2] == v_type:
							for v_j in floor(v_weight): v_available_rooms.push_back(v_i)
							v_already[v_index]=1
					v_index = v_index+1

				v_room.scene_id=v_available_rooms[GLOBAL.RNG.randi_range(0,v_available_rooms.size()-1)]

	func get_room(room_id)->Room:	return rooms[room_id]
	func get_start_room_id()->int:	return maze_id[start_id]-ROOM_ID_OFFSET
		
class Room:
	var id: int										# ROOM INDEX IN MAZE
	var type: int					= ROOM_EMPTY	# ROOM TYPE
	var scene_id: int				= 1				# SCENE ROOM ID
	var width: int					= 1				# ROOM WIDTH
	var height: int					= 1				# ROOM HEIGHT
	var neighbors: Array			= [[],[],[],[]]	# NEIGHBORS ID
	var node: Node					= null			# room CLASS INSTANCE
	func get_scene_file()->String:
		var v_ret = str(scene_id)
		while v_ret.length()<3 : v_ret="0"+v_ret
		return v_ret
	func dump()->String:
		var v_node=" "
		if node!=null : v_node = "X"
		return "(id: "+str(id)+" | "+get_scene_file()+".tscn) ["+str(width)+"x"+str(height)+"] "+\
			str(neighbors)+" (TYPE: "+ROOM_NAMES[type]+") ["+v_node+"]"

func _ready():
	g_maze = Maze.new()
	
	if g_width<4:	g_width = 4
	if g_height<4:	g_height = 4
	
	emit_signal("update", "size", [g_width, g_height], 0)
	
	g_maze.init(g_width, g_height)
	g_maze.build(6)
	g_maze.add_boss()
	g_maze.add_start()
	g_maze.add_treasure()
	g_maze.create_rooms()
	if g_debug : g_maze.dump()
	emit_signal("update","maze", g_maze.maze, 0)
	enter_room(g_maze.get_start_room_id())
	
func get_door_type(_neighbor_type, _type) -> String :
	var v_ret = "none"
	match _type:
		ROOM_BOSS:
			match _neighbor_type:
				ROOM_LAST:		v_ret = "boss"
		_:
			match _neighbor_type:
				ROOM_EMPTY:		pass
				ROOM_BOSS:		v_ret = "boss"
				_:				v_ret = "default"
	return v_ret
	
func get_room(_maze, _id)->Room:
	if _id<0:	return g_empty_room
	else:		return _maze.get_room(_id)
	
func enter_room(_id):
	g_current_room_id = _id
	var v_room = get_room(g_maze, g_current_room_id)
	if g_debug: print("ENTER Room#"+str(g_current_room_id)+" "+v_room.dump())
	if v_room.node==null:
		if g_debug: print("  + Build room")
		v_room.node=		room_scene.instantiate()
		v_room.node.room = v_room.get_scene_file()
		
		if v_room.type == ROOM_BOSS : v_room.node.type="boss"
		
		v_room.node.left=	get_door_type(get_room(g_maze, v_room.neighbors[GLOBAL.DIR.LEFT][0]).type, v_room.type)
		v_room.node.right=	get_door_type(get_room(g_maze, v_room.neighbors[GLOBAL.DIR.RIGHT][0]).type, v_room.type)
		v_room.node.up=		get_door_type(get_room(g_maze, v_room.neighbors[GLOBAL.DIR.UP][0]).type, v_room.type)
		v_room.node.down=	get_door_type(get_room(g_maze, v_room.neighbors[GLOBAL.DIR.DOWN][0]).type, v_room.type)

		v_room.node.connect("room_exit", on_room_exit)
		v_room.node.from= g_from_val	
	else:
		v_room.node.from= g_from_val
		v_room.node.init()
		
	emit_signal("update","upd_room", v_room.id, "current")

	add_child(v_room.node)

func on_room_exit(_dir):
	match(_dir):
		GLOBAL.DIR.NONE:
			GLOBAL.PLAYER.queue_free()
			emit_signal("on_end",null)
		GLOBAL.DIR.BELOW:
			GLOBAL.PLAYER.queue_free()
			emit_signal("on_end",null)
		_:
			var v_enter_dir = [ GLOBAL.DIR.LEFT, GLOBAL.DIR.RIGHT, GLOBAL.DIR.DOWN, GLOBAL.DIR.UP]
			var v_room = g_maze.get_room(g_current_room_id)
			if g_debug: print("EXIT ("+str(_dir)+") Room#"+str(g_current_room_id)+" "+v_room.dump())
			var v_done = "done.default"
			match v_room.type:
				ROOM_BOSS:		v_done = "done.boss"
				ROOM_TREASURE:	v_done = "done.treasure"
			emit_signal("update","upd_room", v_room.id, v_done)
			g_from_val = v_enter_dir[_dir]
			g_next_room_id = v_room.neighbors[_dir][0]
			next_room()

func next_room():
	var v_room = g_maze.get_room(g_current_room_id)
	remove_child(v_room.node)
	enter_room(g_next_room_id)

#func _physics_process(_delta):
#	if Input.is_action_pressed("ui_cancel") && g_bonus_panel : g_bonus_panel.open()

