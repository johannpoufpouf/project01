extends CharacterBody2D
class_name m_generic

var fx_scene 		= preload("res://src/scene/fx.tscn")
var bullet_scene	= preload("res://src/scene/bullet.tscn")
var loot_scene 		= preload("res://src/scene/loot.tscn")

#========= GENERIC DATA FOR ALL MOBS ===========================================
var g_data = {
	"foe"			: true,						# IS FOE
	"life"			: { "value": 4, "max": 4 },
	"force"			: 1,						# BULLET FORCE IF ANY
	"armor"			: 0,						# PROTECTION
	"weight"		: 1,						# WEIGHT OF THE MOB
	"period"		: 0,						# CALL on_period EACH period IN SEC
	"step"			: 0,						# CALL on_step EACH step IN PIXELS²
	"speed"			: 100,
	"accel" 		: 100,
	"friction"		: 0.2,
	"state"			: { "hit" : false}
}
var m_last_step 	= Vector2.ZERO			# POSITION OF THE LAST on_step CALL
var m_time_period	= 0						# DELAY BETWEEN EACH on_period CALL
var m_nb_period		= 0						# on_period CALL COUNTER
var m_hit			= null					# GENERIC HIT

var g_active		= true					# IS ACTIVE

#========= SEND SIGNAL EACH TIME STATS ARE UPDATED =============================
signal update_stats(_what, _data)

func dump():
	if is_foe():	print("[ENNEMY: "+name+"]")
	else:			print("[PLAYER: "+name+"]")
	print("  + active: "+str(is_active()))
	print("  + alive:  "+str(is_alive()))
	print("  + life:   "+str(get_life())+"/"+str(get_life_max()))
	print("  + force:  "+str(get_force()))
	print("  + is hit: "+str(is_hit()))
	print("  + pos:    "+str(position))
	print("  + zindex: "+str(z_index))

#========= ACCESSORS ===========================================================
func set_step(_step) : 			g_data.step = _step
func set_period(_period) : 		g_data.period = _period
func set_weight(_weight):		g_data.weight = _weight
func get_weight():				return g_data.weight
func set_active():				if is_alive(): g_active = true
func set_inactive():			g_active = false
func toggle_active():			g_active = ! g_active && is_alive()
func set_not_foe():				g_data.foe = false
func is_foe():					return g_data.foe
func is_active():				return g_active
func stop():					velocity = Vector2.ZERO
func get_force()->float:		return g_data.force
func set_force(_force):			g_data.force = _force
func mult_force(_ratio):		set_force(g_data.force*_ratio)
func get_armor()->float:		return clamp(g_data.armor,0,90)
func set_armor(_armor):			g_data.armor = _armor
func inc_armor(_value):			g_data.armor = clampi(g_data.armor+_value, 0,75)

#========= CALLBACKS and OVERWRITEN METHODE ====================================
func on_step() 			: 		pass
func on_period(_period)	: 		pass
func get_hitbox()		: 		return null

#========= READY ===============================================================
func _ready() :
	# DEFINE THE HIT (FORCE, EFFECTS, ETC.)
	m_hit = GLOBAL.Hit.new() ; m_hit.node = self
	m_hit.weight = g_data.weight

#========= GENERIC PHYSICS PROCESS =============================================
func _physics_process(_delta):
	if g_data.step :
		if m_last_step == Vector2.ZERO : m_last_step = Vector2(position)
		if (m_last_step - position).length_squared() > g_data.step :
			m_last_step = Vector2(position)
			on_step()
			
	if g_data.period :
		m_time_period = m_time_period + _delta
		if (m_time_period > g_data.period) :
			on_period(m_nb_period)
			m_nb_period = m_nb_period+1
			m_time_period = 0

#========= ACCESSORS ===========================================================
func set_speed(_speed) : g_data.speed = _speed ; emit_signal("update_stats","speed", get_speed())
func set_static():				set_speed(0)
func get_speed():
	var v_ret = g_data.speed
	if g_data.foe:	v_ret = v_ret*GLOBAL.MALUS.speed
	else:			v_ret = v_ret*GLOBAL.BONUS.speed
	return v_ret

func set_accel(_accel) : g_data.accel = _accel ; emit_signal("update_stats","accel", get_accel())
func get_accel():
	var v_ret = g_data.accel
	if g_data.foe:	v_ret = v_ret*GLOBAL.MALUS.speed
	else:			v_ret = v_ret*GLOBAL.BONUS.speed
	return v_ret

func set_friction(_friction) : g_data.friction = _friction ; emit_signal("update_stats","friction", get_friction())
func get_friction():			return g_data.friction

#========= LIFE ================================================================
func set_life(_life:float):
	g_data.life.value = clamp(_life, 0, get_life_max())
	emit_signal("update_stats", "life", get_int_life())
func dec_life(_value:float):	set_life(get_life()-_value)
func inc_life(_value:float):	set_life(get_life()+_value)
func upd_life():				set_life(get_life())
func get_life()->float:			return g_data.life.value
func get_int_life()->int:		return roundi(get_life())
func is_alive()->bool:			return get_int_life()>0


func set_life_max(_max):
	g_data.life.max = clamp(_max,0,get_life_max_max())
	emit_signal("update_stats", "life_max", get_life_max())
func get_life_max_max():		return 9999
func get_life_max():			return g_data.life.max
func is_life_max_max():			return get_life_max()==get_life_max_max()
func inc_life_max(_value):
	if !is_life_max_max():	set_life_max(get_life_max()+_value) ; inc_life(_value)
func dec_life_max(_value):
	if get_life_max()>0:	set_life_max(get_life_max()-_value) ; dec_life(_value)
	
#========= ON_HIT ==============================================================
func set_hit(_is_hit):						g_data.state.hit = _is_hit	
func is_hit():								return g_data.state.hit
func check_hit(_hit : GLOBAL.Hit):			return true
func eval_hit(_hit : GLOBAL.Hit)->float:	return _hit.force*(1-get_armor()/100)
func handle_hit(_hit : GLOBAL.Hit):			set_hit(true) ; apply_hit(eval_hit(_hit))
func apply_hit(_value:float):				g_data.life.value = g_data.life.value - _value
func hit_end()	:							set_hit(false) ; set_active() ; on_hit_end_cbk()
func on_die() :								GLOBAL.STAT.dead = GLOBAL.STAT.dead+1 ; set_life(0) ; on_die_cbk()

func on_hit_end_cbk()	: pass
func on_hit_cbk(_hit)	: hit_end()
func on_die_cbk()		: pass

func _on_hit_area_entered(area):
	if area.owner.has_method("on_hit") && m_hit : area.owner.on_hit(m_hit)

func on_hit(_hit : GLOBAL.Hit) :
	if is_alive() && !is_hit() && check_hit(_hit):
		handle_hit(_hit)
		if is_alive() : on_hit_cbk(_hit)
		else :			on_die()
		if _hit.weight && get_weight() :
			var v_ratio = _hit.weight / get_weight()
			var v_direction = (global_position - _hit.node.global_position).normalized()
			velocity = v_direction * v_ratio * g_data.speed


