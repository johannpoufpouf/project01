extends Node2D

var box_scene	= preload("res://src/scene/objects/box.tscn")

@export_category("Player")
@export_enum("right:0","left:1","up:2","down:3", \
			"right_ex:5","left_ex:6","up_ex:7","down_ex:8", \
			"sky:9","here:10") \
		var from:	int = 2

@export_category("Level")
@export_enum("001", \
			"011","012","013","014","015", "016", "017", "018", "019", "020", \
			"021","022","023", \
			"051","052","053","054", \
			"101","102","103","105", \
			"111", "112", "113", "114", "115", "116", \
			"117", \
			#=== BOSS =======
			"151", \
			#=== TREASURE ===
			"201", "209") \
		var room: String = "001"
@export_enum("default","boss","treasure") var type:				String = "default"
@export_enum("none","default","boss","treasure") var right:		String = "default"
@export_enum("none","default","boss","treasure") var left:		String = "default"
@export_enum("none","default","boss","treasure") var up:		String = "default"
@export_enum("none","default","boss","treasure") var down:		String = "default"

@export_category("Level_ex")
@export_enum("none:0","default:1") var right_ex:int = 0
@export_enum("none:0","default:1") var left_ex:	int = 0
@export_enum("none:0","default:1") var up_ex:	int = 0
@export_enum("none:0","default:1") var down_ex:	int = 0

signal room_exit(_direction)

var g_size		= [1,1]				# LEVEL SIZE
var g_active	= false				# NO FOE DETECTION IS ACTIVE
var g_exit_open = false				# CAN THE PLAYER EXIT
var g_exit_dir	= GLOBAL.DIR.NONE	# EXIT DIRECTION

#======== ACCESSORS ============================================================
func set_active():		g_active = true
func set_inactive():	g_active = false

#======== INITIALISATION =======================================================
func _ready():
	# LOAD LEVEL DATA
	var v_level_scene = load("res://src/scene/rooms/"+room+".tscn")
	var v_level = v_level_scene.instantiate()
	add_child(v_level)
	
	# HANDLE DOORS
	var v_door_scene	= load("res://src/scene/door.tscn")
	
	if type=="boss":
		var v_trap = v_door_scene.instantiate()
		v_trap.position = Vector2(160,96)
		$board/y_sort.add_child(v_trap)
		v_trap.set_type("sewer")
		v_trap.set_dir(GLOBAL.DIR.UP)
		v_trap.idle()
	
	var v_door_pos		= [ Vector2(320,112),Vector2(0,112),Vector2(160,16),Vector2(160,208)]
	var v_door_types	= [ right, left, up, down ]
	var v_door_atlas	= [ [ Vector2i(10,3), Vector2i(14,4) ],
							[ Vector2i(0,3), Vector2i(14,3) ],
							[ Vector2i(5,0), Vector2i(14,1) ],
							[ Vector2i(5,6), Vector2i(14,2) ] ]
	
	for i in 4:
		if v_door_types[i]!="none":
			# MAKE AN HOLE IN THE WALL
			var v_tilemap = $board/background/tilemap
			v_tilemap.set_cell(0, v_door_atlas[i][0], 0, v_door_atlas[i][1])
				
			# PLACE DOORS
			var v_door = v_door_scene.instantiate()
			v_door.position = v_door_pos[i]
			$board/y_sort.add_child(v_door)
			v_door.set_type(v_door_types[i])
			v_door.set_dir(i)
			v_door.idle()
	for v_door in get_tree().get_nodes_in_group(GLOBAL.DOOR_GROUP):
		v_door.connect("door_pass", on_door_pass)
	init()
	GLOBAL.STAT.room = GLOBAL.STAT.room + 1

func init():
	g_exit_open = false
	add_player()
	$animation/def.play("start")

func add_player():
	var v_pos = [ Vector2(286,120),Vector2(34,120),Vector2(160,50),Vector2(160,180) ]
	var v_player = GLOBAL.get_player()
	v_player.position=Vector2(-100,-100)
	v_player.visible = false
	$board/y_sort.add_child(v_player)
	
	v_player.connect("pingmy_end",on_end_player)
	v_player.connect("pingmy_die",on_die_player)
	
	match from:
		0,1,2,3,4,5,6,7,8:
			v_player.set_direction(-1*GLOBAL.DIRVEC[from%4])
			v_player.travel("in")
			v_player.position=v_pos[from]
		9:	v_player.position=Vector2(120,100) ; v_player.travel("fall")
		10: v_player.position=Vector2(120,100) ; v_player.travel("default")

#======== OPEN AND CLOSE DOORS =================================================
func open_doors():
	for v_door in get_tree().get_nodes_in_group(GLOBAL.DOOR_GROUP): v_door.open()
	if type=="boss":
		var v_box = box_scene.instantiate()
		v_box.position = Vector2(160,154)
		v_box.fall()
		$board/y_sort.add_child(v_box)

func close_doors():
	g_exit_open = false
	for v_door in get_tree().get_nodes_in_group(GLOBAL.DOOR_GROUP): v_door.close()
			
#======== MONITOR LEVEL ========================================================
func _process(_delta):
	if g_active && get_tree().get_nodes_in_group(GLOBAL.ENEMY_GROUP).size()==0 :
		g_exit_open = true ; set_inactive() ; $animation/def.play("stop")

#======== PLAYER REACHS EXIT ===================================================
func on_door_pass(_dir, _type):
	if g_exit_open:
		var v_player = GLOBAL.get_player()
		v_player.set_inactive()
		if _type=="sewer":
			g_exit_dir = GLOBAL.DIR.BELOW
			v_player.position = Vector2(160,96)
			v_player.travel("jump")
		else:
			g_exit_dir = _dir
			var v_pos = [ Vector2(296,120),Vector2(24,120),Vector2(160,40),Vector2(160,190) ]
			v_player.travel("out")
			v_player.set_direction(GLOBAL.DIRVEC[g_exit_dir])
			v_player.position = v_pos[g_exit_dir]
		
		$animation/def.play("end")

func on_end():
	var v_player = GLOBAL.get_player()
	v_player.stop()
	v_player.disconnect("pingmy_die",on_die_player)
	v_player.disconnect("pingmy_end",on_end_player)
	
	$board/y_sort.remove_child(v_player)
	emit_signal("room_exit", g_exit_dir)

func on_die_player():
	for mob in get_tree().get_nodes_in_group(GLOBAL.ENEMY_GROUP) : mob.set_inactive()
	
func on_end_player():
	g_exit_dir = GLOBAL.DIR.NONE
	$animation/def.play("end")
