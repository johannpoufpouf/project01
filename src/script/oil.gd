extends m_generic

@onready var g_anim =					$animation/tree
@onready var g_state =					g_anim["parameters/playback"]

#======== GENERIC CALLBACK =====================================================
func on_hit_cbk(_hit : GLOBAL.Hit):		$sprite/fx_hit.start()
func _on_fx_hit_on_finished():			hit_end()
func on_die_cbk():						$sprite/fx_hit.start() ; g_state.travel("die")
func on_end(): 							queue_free()

#======== ATTRIBUTES ===========================================================
@export var fire: int = 3				# FIRE FREQUENCY 1/n JUMP
@export var life: int = 8				# LIFE
@export var number: int = 4				# NUMBER OF FIRES
@export var offset: int = 0				# FIRE OFFSET ANGLE
var m_fire_wait = 1						# REBOUND WAIT BEFORE FIRE 
var m_fire = 0							# FIRE EVERY NB REBOUND
var m_velocity = Vector2.ZERO			# OIL VELOCITY VECTOR

#======== INITIALISATION =======================================================
func _ready():
	super()
	set_life_max(life) ; set_life(life)
	set_period(2.4) ; set_weight(2) ; set_speed(10) ; set_accel(30)
	m_hit.node = $hit ; m_hit.weight = 2 ; m_hit.force = 2

#======== MOVE RANDOMLY ========================================================
func _physics_process(_delta):
	if !is_active(): return
	super(_delta)
	if is_alive() :
		velocity = velocity.move_toward(m_velocity*get_speed(), get_accel()*_delta)
		move_and_slide()
	
#======== SHOOT ================================================================
func shoot():
	if !is_active() : return
	if m_fire_wait > 0 : m_fire_wait = m_fire_wait-1; return
	m_fire = m_fire+1
	var v_splash = fx_scene.instantiate()
	v_splash.set_type("splash",0)
	v_splash.set_position(position)
	v_splash.modulate = Color.BLACK
	v_splash.z_index = 2
	v_splash.rand_flip()
	get_parent().add_child(v_splash)
	
	if (m_fire%fire == 0) :
		m_fire = 0
		for i in number:
			var v_offset = 2*PI*offset/360
			var v_dir = Vector2(1,0).rotated(2*PI*i/number+v_offset)
			var v_bullet = bullet_scene.instantiate()
			var v_position = Vector2.ZERO
			var v_height = 15
			v_position.y = position.y + v_dir.y*4
			v_position.x = position.x + v_dir.x*6
			v_bullet.set_initial_height(v_height)
			v_bullet.set_position(v_position)
			v_bullet.set_direction(v_dir)
			v_bullet.set_foe()
			v_bullet.set_speed(100)
			if GLOBAL.MALUS.fire_size!=1: v_bullet.scale = Vector2(GLOBAL.MALUS.fire_size,GLOBAL.MALUS.fire_size)
			v_bullet.modulate = Color.BLACK
			get_parent().add_child(v_bullet)

#======== ON PERIOD CALLBACK ===================================================
func on_period(_period)	:
	if is_alive():
		m_velocity = Vector2(1,0).rotated(GLOBAL.RNG.randf_range(0,2*PI))
		g_anim.set("parameters/run/blend_position", m_velocity.x)
		g_anim.set("parameters/die/blend_position", m_velocity.x)
	
#======== ON HIT ===============================================================
func _on_hit_area_entered(area): super(area) ; on_die()

