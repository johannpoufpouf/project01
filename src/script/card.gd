extends Node2D

@export_category("Card")
@export_enum("anar","anonymous","bbq","beer","crap","garbage",
	"googles","hook","megaphone","molotov","music","peace","revolution",
	"rock","sign","speed_down","speed_up","tag","yellow") \
		var type: String = "anar"
		
@export var autostart:bool = false
		
var g_is_hidden:bool = true
var g_card = null
		
#======== INITIALISATION =======================================================
func _ready():
	var v_card_scene = load("res://src/scene/cards/"+type+".tscn")
	g_card = v_card_scene.instantiate()
	g_card.position=Vector2(-24,-32)
	$front.add_child(g_card)
	if autostart: open() ; play()
	
	
#======== HANDLE ===============================================================
func gray()->void: modulate=Color.DARK_GRAY
func white()->void: modulate=Color.WHITE
func play()->void: g_card.get_node("def").play("default")
func stop()->void: g_card.get_node("def").pause()
func open()->void:	g_is_hidden = false ; $def.play("open")
func close()->void:	g_is_hidden = true  ; $def.play("close")
func is_hidden()->bool: return g_is_hidden
	

