extends Node2D

var card_scene	= preload("res://src/scene/card.tscn")

@export var autostart:bool		= false

@export_category("Bonus")
@export var nb_bonus:int		= 0
@export var second_ratio:int	= 0
@export var hidden_ratio:float	= 0
@export var second_hidden:bool	= true
@export var malus_ratio:float	= 0

@export_group("Bonus set")
@export_flags("anar","anonymous","bbq","beer",
	"googles","hook","megaphone","molotov","music","peace","revolution",
	"rock","sign","speed_up","tag","yellow") \
		var bonus = -1
var g_bonus:Array = [ "anar","anonymous","bbq","beer",
	"googles","hook","megaphone","molotov","music","peace","revolution",
	"rock","sign","speed_up","tag","yellow" ]

@export_group("Malus set")
@export_flags("crap","garbage","speed_down") \
		var malus = 255	
var g_malus:Array = [ "crap","garbage","speed_down" ]

var g_first_cards:Array = []
var g_second_cards:Array = []

var g_offset = 0
var g_current = -1

var g_available:bool=false
var g_timer:float

enum STATE {NONE, CHOICE, MOVE, OPEN, DELAY, WAIT, OUTRO, END}
var g_state:STATE = STATE.NONE
const g_final_pos=[160,100,220]

#======== READY METHOD =========================================================
func _ready():
	GLOBAL.BONUS_SCENE=self
	$pointers.visible=false
	visible=false
	if autostart: init(true)

#======== CLEAR ALL PREVIOUS CARDS =============================================
func clear():
	for c in get_tree().get_nodes_in_group(GLOBAL.CARD_GROUP): c.queue_free()
	g_current = -1
	g_first_cards.clear()
	g_second_cards.clear()
	$fireworks01.visible=false ; $fireworks01.emitting=false
	$fireworks02.visible=false ; $fireworks02.emitting=false
	$fireworks03.visible=false ; $fireworks03.emitting=false
	visible=false

#======== INITIALISATION =======================================================
func init(_open:bool):
	clear()
	
	if nb_bonus==0:
		nb_bonus = 2
		if GLOBAL.PLAYER:
			var v_nb_bonus_max = 2
			var v_nb_bonus_min = 2
			if GLOBAL.PLAYER.get_luck()>=1: v_nb_bonus_max = 3
			if GLOBAL.PLAYER.get_luck()>=5: v_nb_bonus_max = 4
			if GLOBAL.PLAYER.get_luck()>=7: v_nb_bonus_min = 3
			nb_bonus=GLOBAL.RNG.randi_range(v_nb_bonus_min, v_nb_bonus_max)
	
	if second_ratio<1:
		second_ratio=5
		if GLOBAL.PLAYER:
			if GLOBAL.PLAYER.get_luck()>=2: second_ratio = 4
			if GLOBAL.PLAYER.get_luck()>=3: second_ratio = 3
			if GLOBAL.PLAYER.get_luck()>=6: second_ratio = 2
			if GLOBAL.PLAYER.get_luck()>=8: second_ratio = 1
			
	if hidden_ratio<1:
		hidden_ratio=1.5
		second_hidden = true
		if GLOBAL.PLAYER:
			if GLOBAL.PLAYER.get_luck()>=2: hidden_ratio = 1.3
			if GLOBAL.PLAYER.get_luck()>=5: second_hidden = false
			if GLOBAL.PLAYER.get_luck()>=7: hidden_ratio = 1.15
			if GLOBAL.PLAYER.get_luck()>=9: hidden_ratio = 1
			
	if malus_ratio<1:
		malus_ratio = 2.5
		if GLOBAL.PLAYER: malus_ratio = 2.5 + GLOBAL.PLAYER.get_luck()/4
	
	g_offset = 0 ; if nb_bonus==2: g_offset=1
	
	$pointers.visible=false

	# PREPARE AVAILABLE BONUS AND MALUS FOR FIRST AND SECOND CARD	
	var v_available_first_cards=[]
	var v_available_second_cards=[]
	var v_id=1
	
	var v_bonus_size = 0
	var v_malus_size = 0
	for v_i in range(g_bonus.size()): if bonus&(1<<v_i)!=0 : v_bonus_size = v_bonus_size+1
	for v_i in range(g_malus.size()): if malus&(1<<v_i)!=0 : v_malus_size = v_malus_size+1

	for v_bonus in g_bonus:
		if bonus==-1 || bonus&v_id!=0 :
			for j in range(malus_ratio*v_malus_size):
				v_available_first_cards.push_back(v_bonus)
				v_available_second_cards.push_back(v_bonus)
		v_id=v_id*2
	v_id=1
	for v_malus in g_malus:
		if malus==-1 || malus&v_id!=0 :
			for j in range(v_bonus_size):
				v_available_first_cards.push_back(v_malus)
				for k in range(2): v_available_second_cards.push_back(v_malus)
	if v_available_first_cards.size()==0: v_available_first_cards.push_back("none")
	
	# CREATE BONUS
	for i in range(nb_bonus):
		get_node("cards"+str(i+1+g_offset)).visible=true
		
		# MAIN BONUS
		var v_card = card_scene.instantiate()
		v_card.z_index=1
		v_card.gray()
		v_card.autostart = false
		v_card.type=v_available_first_cards[GLOBAL.RNG.randi_range(0,v_available_first_cards.size()-1)]
		get_node("cards"+str(i+1+g_offset)).add_child(v_card)
		g_first_cards.push_back(v_card)
	
		# SECOND CARD
		if GLOBAL.RNG.randf_range(0,second_ratio)<1:
			var v_second_card = card_scene.instantiate()
			v_second_card.autostart = false
			v_second_card.position=Vector2(-10,5)
			v_second_card.gray()
			v_second_card.type=v_available_second_cards[GLOBAL.RNG.randi_range(0,v_available_second_cards.size()-1)]
			get_node("cards"+str(i+1+g_offset)).add_child(v_second_card)
			g_second_cards.push_back(v_second_card)
		else: g_second_cards.push_back(null)
	if _open:open()

#======== SHOW CARDS ACCORDING TO HIDDEN RATIO =================================
func show_cards():
	if !DisplayServer.is_touchscreen_available ( ):
		$cards1/key.visible = true
		$cards2/key.visible = true
		$cards3/key.visible = true
		$cards4/key.visible = true
	
	for i in g_first_cards.size():
		if GLOBAL.RNG.randf_range(0,hidden_ratio)<1: g_first_cards[i].open()

		# DO NOT SHOW SECOND IF FIRST IS HIDDEN (THAT WOULD BE ODD)
		if !second_hidden && g_second_cards[i] && ! g_first_cards[i].is_hidden() && GLOBAL.RNG.randf_range(0,hidden_ratio)<1:
			g_second_cards[i].open()


func set_available(): g_available=true; g_state = STATE.CHOICE

#======== OPEN BONUS PANEL AND DROP ITEMS ======================================
func open():
	get_tree().paused = true
	$animation/def.play("drop"+str(nb_bonus))

func on_close_end():
	if GLOBAL.PLAYER:
		GLOBAL.PLAYER.on_new_bonus(g_first_cards[g_current].type)
		if g_second_cards[g_current]:
			GLOBAL.PLAYER.on_new_bonus(g_second_cards[g_current].type)
	clear()
	get_tree().paused = false

#======== HANDLE PHASES ========================================================
func _physics_process(_delta):
	if g_available:
		if g_timer>0: g_timer = g_timer-_delta
		if g_timer>0: return
		match g_state:
			STATE.CHOICE:				
				var v_keys=["ui_fire_left","ui_fire_up","ui_fire_down","ui_fire_right"]
				for v_slot in range(v_keys.size()) :
					if Input.is_action_pressed(v_keys[v_slot]) :
						if v_slot>=g_offset && v_slot<nb_bonus+g_offset:
							if g_current!=v_slot-g_offset:	select_slot(v_slot-g_offset)
							else: done()
				var v_id = g_current
				if Input.is_action_pressed("ui_left"):
					if v_id==-1 : v_id=nb_bonus-1
					elif v_id>0: v_id = v_id-1
					select_slot(v_id)
				elif Input.is_action_pressed("ui_right"):
					if v_id==-1 : v_id = 0
					elif v_id<nb_bonus-1: v_id = v_id+1
					select_slot(v_id)
				elif g_current!=-1 && (Input.is_action_pressed("ui_up")||Input.is_action_pressed("ui_down")): done()
			STATE.MOVE:
				var v_pos_id=0
				var v_finish=0
				const g_speed=300
				
				if g_second_cards[g_current]:
					v_pos_id=1
					var v_second_diff = g_second_cards[g_current].global_position.x-g_final_pos[2]
					if v_second_diff<0 && g_current+g_offset!=3: g_second_cards[g_current].position.x = g_second_cards[g_current].position.x + g_speed*_delta
					elif v_second_diff>0 && g_current+g_offset==3:g_second_cards[g_current].position.x = g_second_cards[g_current].position.x - g_speed*_delta
					else: v_finish=1
				var v_first_diff = g_first_cards[g_current].global_position.x-g_final_pos[v_pos_id]
				if v_first_diff<0 && g_current+g_offset+v_pos_id<2: g_first_cards[g_current].position.x = g_first_cards[g_current].position.x + g_speed*_delta
				elif v_first_diff>0 && g_current+g_offset+v_pos_id>1: g_first_cards[g_current].position.x = g_first_cards[g_current].position.x - g_speed*_delta
				else: v_finish=v_finish+1
				
				if v_finish>v_pos_id: g_state=STATE.OPEN
			STATE.OPEN:
				$fireworks01.visible=true ; $fireworks01.emitting=true
				$fireworks02.visible=true ; $fireworks02.emitting=true
				$fireworks03.visible=true ; $fireworks03.emitting=true
				if g_first_cards[g_current].is_hidden(): g_timer=1;g_first_cards[g_current].play();g_first_cards[g_current].open()
				if g_second_cards[g_current] && g_second_cards[g_current].is_hidden(): g_timer=1; g_second_cards[g_current].play();g_second_cards[g_current].open()
				g_state=STATE.WAIT
			STATE.WAIT:
				if Input.is_anything_pressed():
					$animation/def.play("close")
					g_available=false

#======== CARD AND SLOT SELECTION ==============================================
func select_card(_card, _selected):
	if _card:
		if _selected:	_card.play(); _card.white()
		else:			_card.stop(); _card.gray()

func select_slot(_id):
	if g_current!=-1 :
		get_node("cards"+str(g_current+g_offset+1)).scale=Vector2(1,1)
		select_card(g_first_cards[g_current], false)
		select_card(g_second_cards[g_current], false)
	select_card(g_first_cards[_id], true)
	select_card(g_second_cards[_id], true)
	get_node("cards"+str(_id+g_offset+1)).scale=Vector2(1.1,1.1)
	$pointers.position=Vector2(40+(_id+g_offset)*75,100)
	$pointers.visible = true
	g_current=_id
	g_timer=0.2

#======== SLOT IS VALIDATED ====================================================
func done():
	g_state=STATE.MOVE
	for i in range(4):
		if i!=g_current+g_offset: get_node("cards"+str(i+1)).visible=false
	get_node("cards"+str(g_current+g_offset+1)).scale=Vector2(1,1)
	get_node("cards"+str(g_current+1+g_offset)+"/key").visible=false
	$pointers.visible=false
	
