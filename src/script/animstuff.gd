extends Node2D

@export_category("Backdrop")
@export_enum("bubble_1", "bubble_2", "bubble_3", \
	"bubble_11", "bubble_12",\
	"floater_1", "floater_2") \
	var what: String = "bubble_1"
		
func _ready():
	match what:
		"floater_1", "floater_2" : _on_delay_timeout()
		_: $delay.start(GLOBAL.RNG.randf_range(0,2))

func _on_delay_timeout(): $animation/def.play(what)
