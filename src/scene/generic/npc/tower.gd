extends m_generic

@onready var g_anim =					$animation/tree
@onready var g_state =					g_anim["parameters/playback"]

@export var nb_fires: int = 1				# NUMBER OF FIRES
@export var nb_bullets: int = 8				# NUMBER OF BULLETS
@export var angle: int = 360				# FIRE ANGLE RANGE
@export var offset: int = 0					# FIRE ANGLE OFFSET

var g_upper: int = 0						# PLAYER UP

#======== GENERIC CALLBACK =====================================================
func on_hit_cbk(_hit : GLOBAL.Hit):		g_state.travel("hit")
func on_die_cbk():						g_state.travel("die")
func on_end():							remove_from_group(GLOBAL.ENEMY_GROUP)

#======== INITIALISATION =======================================================
func _ready():
	super()
	set_period(5); set_life(3)
	
#======== ON PERIOD CALLBACK ===================================================
func on_period(_period)	:
	if is_alive() && g_upper<=0 :
		g_state.travel("open_"+str(nb_fires))

#======== SHOOT ================================================================
func shoot():
	if !is_active(): return
	
	for i in nb_bullets:
		var v_dir = Vector2(1,0).rotated(2*PI/360*(angle*i/nb_bullets+offset))
		var v_bullet = bullet_scene.instantiate()
		var v_position = Vector2.ZERO
		var v_height = 15
		v_position.y = position.y + v_dir.y*4
		v_position.x = position.x + v_dir.x*6
		v_bullet.set_initial_height(v_height)
		v_bullet.set_position(v_position)
		v_bullet.set_direction(v_dir)
		v_bullet.set_foe()
		v_bullet.set_speed(100)
		v_bullet.set_distance(800)
		v_bullet.modulate = Color.RED
		get_parent().add_child(v_bullet)

func _on_detect_body_entered(_body): g_upper = g_upper+1
func _on_detect_body_exited(_body): g_upper = g_upper-1
